package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;


import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.CustomerDetailsDo;
import com.tbs.generic.vansales.Model.CustomerDetailsMainDo;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class CustomerDetailsRequest extends AsyncTask<String, Void, Boolean> {

    private CustomerDetailsMainDo customerDetailsMainDo;
    private Context mContext;
    private String id;
    String customercode, addresscode, ip, pool, port;
    int type;
    PreferenceUtils preferenceUtils;

    public CustomerDetailsRequest(String customerCode,String addresscode,int type, Context mContext) {

        this.mContext = mContext;
        this.customercode = customerCode;
        this.addresscode = addresscode;
        this.type = type;

    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, CustomerDetailsMainDo customerDetailsMainDo);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_TYP", type);

            jsonObject.put("I_BPR", customercode);
            jsonObject.put("I_ADDCOD", addresscode);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.CUSTOMER_DETAILS, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }


    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            customerDetailsMainDo = new CustomerDetailsMainDo();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {

                        if (attribute.equalsIgnoreCase("O_CARRIER")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.carrier = text;
                            }


                        } else if (attribute.equalsIgnoreCase("O_LICENUM")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.licence = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_LICETYP")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.licenceType=text;
                            }


                        }
                        else if (attribute.equalsIgnoreCase("O_CATEG")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.category=text;
                            }


                        }
                        else if (attribute.equalsIgnoreCase("O_YCUR")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.currency=text;
                            }


                        } else if (attribute.equalsIgnoreCase("O_DES")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.descrption=text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_TAXRULE")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.tax=text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_BTC")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.billtocustomer=text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_PBC")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.paybycustomer = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_BL")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.businessline= text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_ADD")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.address = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_POSCOD")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.postalcode = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_CTY")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.city = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_TEL")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.telephone=text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_MOB")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.mobile=text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_EMI")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.email=text;
                            }

                        }

                         else if (attribute.equalsIgnoreCase("O_YTEL")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.telephone=text;
                            }



                        } else if (attribute.equalsIgnoreCase("O_YMOB")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.mobile=text;
                            }



                        }
                        else if (attribute.equalsIgnoreCase("O_FLAG")) {
                            if (text.length() > 0) {

                                customerDetailsMainDo.flag= Integer.parseInt(text);
                            }



                        }

                        text = "";
                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }



                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, customerDetailsMainDo);
        }
    }
}