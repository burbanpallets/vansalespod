package com.tbs.generic.vansales.Activitys

import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatDialog
import com.tbs.generic.vansales.Adapters.InvoiceAdapter
import com.tbs.generic.vansales.Adapters.PurchaseRecieptInfoAdapter
import com.tbs.generic.vansales.Adapters.SerialSelectionAdapter
import com.tbs.generic.vansales.Model.ActiveDeliveryDO
import com.tbs.generic.vansales.Model.SerialListDO
import com.tbs.generic.vansales.Model.StockItemDo
import com.tbs.generic.vansales.Model.TrailerSelectionDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.CreateLotRequest
import com.tbs.generic.vansales.Requests.SaveSerialsRequest
import com.tbs.generic.vansales.Requests.UnPaidInvoicesListRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.activity_equipserial_number_barcode.*
import kotlinx.android.synthetic.main.activity_equipserial_number_barcode.tvBookedQty
import kotlinx.android.synthetic.main.activity_equipserial_number_barcode.tv_product
import kotlinx.android.synthetic.main.activity_serial_number_allocation.*
import kotlinx.android.synthetic.main.configuration_layout.*
import java.lang.Double
import java.util.*


class BarcodeAddActivity : BaseActivity() {

    lateinit var invoiceAdapter: InvoiceAdapter
    var recieptDos: ArrayList<ActiveDeliveryDO> = ArrayList()
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
    lateinit var view: LinearLayout
    lateinit var btnConfirm: Button

    var fromId = 0
    var productId = ""
    var productDescription = ""
    lateinit var activeDeliveryDO: TrailerSelectionDO
    lateinit var adapter: SerialSelectionAdapter

    private var initAL: String? = null // keep starting AN
    lateinit var stockDos: ArrayList<SerialListDO>

    var position: Int = 0


    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.activity_equipserial_number_barcode, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()

        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            finish()
        }
        initializeControls()
        this.initAL = initAL
        if (intent.hasExtra("PRODUCTDO")) {
            activeDeliveryDO = intent.extras!!.getSerializable("PRODUCTDO") as TrailerSelectionDO
        }
        if (intent.hasExtra("POSITION")) {
            position = intent.extras!!.getInt("POSITION")
        }
        tv_product.setText(activeDeliveryDO.trailer + " - " + activeDeliveryDO.trailerDescription)
        tvBookedQty.setText("" + activeDeliveryDO.quantity)
        var stockDos = StorageManager.getInstance(this).getSerialList(this)
       if(stockDos.size>0){
           var itemDo = StockItemDo()
           adapter = SerialSelectionAdapter(itemDo, activeDeliveryDO, this@BarcodeAddActivity, stockDos)
           recycleview.adapter = adapter
           tvNoDataFound.visibility=View.GONE
       }

//
//        toolbar.setNavigationIcon(R.drawable.back)
//        toolbar.setNavigationOnClickListener {
//            showToast(getString(R.string.lotmanagement))
//
//        }
    }

    private fun saveSerials() {

        if (Util.isNetworkAvailable(this)) {
            showLoader()
            var stockDos = StorageManager.getInstance(this).getSerialList(this)

            val request = SaveSerialsRequest(
                    this,
                    stockDos, activeDeliveryDO
            )
            request.setOnResultListener { isError, contarctMainDO ->
                hideLoader()
                if (isError) {
                    showToast(resources.getString(R.string.server_error));

                } else {
                    if (contarctMainDO != null) {
                        if (contarctMainDO.flag == 20) {
                            var equipDos = StorageManager.getInstance(this).getSerialEquipmentSelectionDO(this)
//                            val itemIndex: Int = equipDos.indexOf(ac)

                            activeDeliveryDO.isUpdated = 2
                            equipDos.set(position, activeDeliveryDO)

                            StorageManager.getInstance(this).saveSerialEquipmentSelectionDO(this, equipDos)

                            showToast(contarctMainDO.successFlag)
                            finish()
//                           modify(contarctMainDO.successFlag)
                        } else {
                            showToast(resources.getString(R.string.server_error));

                        }

                    } else {
                        showToast(resources.getString(R.string.server_error));

                    }
                }

            }
            request.execute()
        } else {
            showToast(resources.getString(R.string.internet_connection));
        }
    }

    override fun initializeControls() {
//        tvScreenTitle.setText("Enter Details")
        tv_title.setText(resources.getString(R.string.save_serials))

        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.rcv_stock_selection)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById<TextView>(R.id.tvNoDataFound)
        var stockDos = StorageManager.getInstance(this).getSerialList(this)

        if (stockDos.size > 0) {
            tvNoDataFound.visibility = View.GONE

        }
        ivAdd.visibility = View.GONE

        btnScan.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(applicationContext, TestBarcodeActivity::class.java)

            startActivityForResult(intent, 11)
        }
        btnConfirm = findViewById<Button>(R.id.btn_save)
        recycleview.layoutManager = linearLayoutManager
        var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

        tvScreenTitle.text = activeDeliverySavedDo.customerDescription
        btnConfirm.setOnClickListener {
            var stockDos = StorageManager.getInstance(this).getSerialList(this)

            if (stockDos.size == activeDeliveryDO.quantity) {
                saveSerials()
            } else {
                showToast(resources.getString(R.string.qty_notmatchingg))

            }

        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var stockDos = StorageManager.getInstance(this).getSerialList(this)

        if (requestCode == 11 && resultCode == 11) {
            var serial = data!!.getStringExtra("SERIAL")
            var stockItemDo = SerialListDO()
            stockItemDo.serialDO = serial
            stockDos.add(stockItemDo)
            var itemDo = StockItemDo()
            StorageManager.getInstance(this).saveSerialList(this,stockDos)
            adapter = SerialSelectionAdapter(itemDo, activeDeliveryDO, this@BarcodeAddActivity, stockDos)
            recycleview.adapter = adapter
        }
        if (stockDos.size > 0) {
            tvNoDataFound.visibility = View.GONE
        }
    }

    override fun onBackPressed() {
        finish()
//        showAppCompatAlert("Alert!", getString(R.string.confirm_serial), getString(R.string.ok), getString(R.string.cancel), "ALERT", true)
    }

    override fun onButtonYesClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            val siteListRequest = CreateLotRequest(recieptDos, this@BarcodeAddActivity)
            siteListRequest.setOnResultListener { isError, approveDO, msg ->
                hideLoader()

                if (isError) {
                    if (msg.isNotEmpty()) {
                        showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.failure), false)

                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)

                    }
                } else {
                    if (approveDO.flag == 2) {
                        val intent = Intent()
                        intent.putExtra("RecieptDOS", recieptDos)
                        setResult(10, intent)
                        finish()
                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)
                    }

                }

            }
            siteListRequest.execute()

        } else
            if (getString(R.string.failure).equals(from, ignoreCase = true)) {

            }

    }

}