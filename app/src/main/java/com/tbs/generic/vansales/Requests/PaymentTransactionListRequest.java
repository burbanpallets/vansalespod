package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.TransactionCashDO;
import com.tbs.generic.vansales.Model.TransactionCashMainDo;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class PaymentTransactionListRequest extends AsyncTask<String, Void, Boolean> {

    private TransactionCashMainDo transactionMainDo;
    private TransactionCashDO transactionCashDO;

    private Context mContext;
    PreferenceUtils preferenceUtils;
    String startDATE, endDATE;


    public PaymentTransactionListRequest(String startdate, String endDate, Context mContext) {

        this.mContext = mContext;
        this.startDATE = startdate;
        this.endDATE = endDate;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, TransactionCashMainDo unPaidInvoiceMainDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_YUSER", id);
            jsonObject.put("I_YSTARTDATE", startDATE);
            jsonObject.put("I_ENDDATE", endDATE);
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.PAYMENTS_CASH, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }
    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            transactionMainDo = new TransactionCashMainDo();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP2")) {
                        transactionMainDo.transactionCashDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("GRP6")) {
//                        transactionMainDo.transactionChequeDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("TAB")) {


                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        transactionCashDO = new TransactionCashDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_CUST")) {
                            transactionCashDO.cashCustomer = text;


                        } else if (attribute.equalsIgnoreCase("O_AMT")) {
                            if (text.length() > 0) {

                                transactionCashDO.cashAmount = Double.valueOf(text);
                            }

                        }  else if (attribute.equalsIgnoreCase("O_PAYTYP")) {
                            if (text.length() > 0) {

                                transactionCashDO.payType = text;
                            }

                        }  else if (attribute.equalsIgnoreCase("O_PAYNUM")) {
                            if (text.length() > 0) {

                                transactionCashDO.cashPayment = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YTOTCASHPAY")) {
                            if (text.length() > 0) {

                                transactionMainDo.cashReciepts = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YTOTCASHAMT")) {
                            if (text.length() > 0) {

                                transactionMainDo.totalAmount = Double.valueOf(text);
                            }

                        }

                    }



                    if (endTag.equalsIgnoreCase("LIN")) {

                        transactionMainDo.transactionCashDOS.add(transactionCashDO);

                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    if (xpp.getText().length() > 0) {

                        text = xpp.getText();
                    } else {
                        text = "";
                    }
                }


                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();
        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        ((BaseActivity) mContext).hideLoader();

        //  ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, transactionMainDo);
        }
    }
}