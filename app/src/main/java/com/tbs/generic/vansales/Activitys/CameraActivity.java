package com.tbs.generic.vansales.Activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.tbs.generic.vansales.R;


public class CameraActivity extends Activity implements SurfaceHolder.Callback, View.OnClickListener {

    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;

    WindowManager.LayoutParams lp;
    private ImageView flashlight;
    private boolean flashmode = false;
    private Camera camera;


    Context mContext=this;

    private ImageView CAPTURE;
    private ImageView LINE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

CAPTURE=(ImageView)findViewById(R.id.capture_pic);
        LINE=(ImageView)findViewById(R.id.anim_line);
        CAPTURE.setOnClickListener(this);


        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        surfaceHolder = surfaceView.getHolder();

        surfaceHolder.addCallback(this);

        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = 10 / 100.0f;
        getWindow().setAttributes(lp);

        flashlight=(ImageView)findViewById(R.id.flash);
        flashlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flashOnButton();
            }
        });










    }


    public static ColorMatrixColorFilter brightIt(int fb) {
        ColorMatrix cmB = new ColorMatrix();
        cmB.set(new float[] {
                1, 0, 0, 0, fb,
                0, 1, 0, 0, fb,
                0, 0, 1, 0, fb,
                0, 0, 0, 1, 0   });

        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.set(cmB);
//Canvas c = new Canvas(b2);
//Paint paint = new Paint();
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(colorMatrix);
//paint.setColorFilter(f);
        return f;
    }



    private void flashOnButton() {
        flashlight.setImageResource(R.drawable.camera);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = 50 / 100.0f;
        getWindow().setAttributes(lp);
        if (camera != null) {
            try {
                Camera.Parameters param = camera.getParameters();
                param.setFlashMode(!flashmode ? Camera.Parameters.FLASH_MODE_TORCH
                        : Camera.Parameters.FLASH_MODE_OFF );


                camera.setParameters(param);
                flashmode = !flashmode;
            } catch (Exception e) {
                // TODO: handle exception
            }

        }
        if (!flashmode){
            flashlight.setImageResource(R.drawable.cameranew);
            lp = getWindow().getAttributes();
            lp.screenBrightness = 10 / 100.0f;
            getWindow().setAttributes(lp);
        }

    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            camera = Camera.open();
            camera.setDisplayOrientation(90);
        } catch (RuntimeException e) {
            System.err.println(e);

            return;

        }

        Camera.Parameters param;

        param = camera.getParameters();
        camera.setParameters(param);

        try {

            camera.setPreviewDisplay(surfaceHolder);

            camera.startPreview();

        } catch (Exception e) {

            System.err.println(e);

            return;

        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        refreshCamera();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        camera.stopPreview();

        camera.release();
        camera = null;

    }
    public void refreshCamera() {
        if (surfaceHolder.getSurface() == null) {
            return;
        }
        try {
            camera.stopPreview();
        } catch (Exception e) {

        }
        try {
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
        } catch (Exception e) {

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.capture_pic:
                LINE.setVisibility(View.VISIBLE);
                DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
                int width = displayMetrics.widthPixels;
                int height = displayMetrics.heightPixels;
                TranslateAnimation animation = new TranslateAnimation(0.0f, 0.0f, height - 30, 0.0f);
                animation.setDuration(2500);
                animation.setRepeatCount(100);
                animation.setRepeatMode(2);
                animation.setFillAfter(true);
                LINE.startAnimation(animation);
                Handler handler=new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        startActivity(new Intent(getApplicationContext(),Sample.class));
                    }
                },5000);
                break;
            case R.id.anim_line:

                break;
        }
    }
}
