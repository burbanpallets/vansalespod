package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class MasterDataRequest extends AsyncTask<String, Void, Boolean> {

    ArrayList<CustomerDo> customers;

    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    int type;
    PreferenceUtils preferenceUtils;

    public MasterDataRequest(Context mContext, int type) {
        this.type = type;

        this.mContext = mContext;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, ArrayList<CustomerDo> customers);

    }

    public boolean runRequest() {

        preferenceUtils = new PreferenceUtils(mContext);
        String site = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_TYP", type);

            jsonObject.put("I_SITE", site);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.MASTER_DATA, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }


    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();
            CustomerDo customer = null;
            customers = new ArrayList<>();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

//
                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        customer = new CustomerDo();
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();
                    if (startTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_BPR")) {
                            if (text.length() > 0) {
                                customer.customer = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_BPRNAM")) {
                            if (text.length() > 0) {

                                customer.customerName = text;
                            }
                        }  else if (attribute.equalsIgnoreCase("O_YBPFLG")) {
                            if (text.length() > 0) {

                                customer.flag = Integer.parseInt(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_CTY")) {
                            if (text.length() > 0) {

                                customer.city = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_CRY")) {
                            if (text.length() > 0) {

                                customer.countryName = text;
                            }
                        }
                        text = "";


                    }
                    if (endTag.equalsIgnoreCase("LIN")) {
                        customers.add(customer);
                    }
                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();


                }
                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception " + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, customers);
        }
    }
}