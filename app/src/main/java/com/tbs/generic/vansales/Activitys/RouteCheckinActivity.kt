package com.tbs.generic.vansales.Activitys

import android.app.Dialog
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Model.DriverIdMainDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.utils.PreferenceUtils


class RouteCheckinActivity : BaseActivity() {
    lateinit var tvSelection:TextView
    lateinit var dialog:Dialog
    lateinit var pickDos: DriverIdMainDO

    override fun initialize() {
      var  llCategories = layoutInflater.inflate(R.layout.create_invoice_screen, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.invoice)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        tvSelection           = findViewById<TextView>(R.id.tvSelection)
        tvSelection.text = getString(R.string.select_site)

        preferenceUtils = PreferenceUtils(this)

    }



}