package com.tbs.generic.vansales.Activitys

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Adapters.DamageReasonListAdapter
import com.tbs.generic.vansales.Adapters.InvoiceAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.DamageReasonsListRequest
import com.tbs.generic.vansales.utils.PreferenceUtils
import java.util.ArrayList


class DamageReasonsActivity : BaseActivity() {


    lateinit var invoiceAdapter: InvoiceAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound:TextView
    var fromId= 0

    override fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = layoutInflater.inflate(R.layout.damage_reasons_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            var intent = Intent()
            var id = preferenceUtils.getIntFromPreference(PreferenceUtils.DAMAGE_REASON_ID, 0)
            intent.putExtra("data", id)
            setResult(143, intent)
            finish()
        }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.reason)
        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById<TextView>(R.id.tvNoDataFound)

        recycleview.layoutManager = linearLayoutManager

//        invoiceAdapter = InvoiceAdapter(this@SalesInvoiceActivity,  null)
//        recycleview.setAdapter(invoiceAdapter)
//
        val siteListRequest = DamageReasonsListRequest(this@DamageReasonsActivity)
        siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
            hideLoader()
            if (unPaidInvoiceMainDO != null) {
                if (isError) {
                    showAppCompatAlert("", getString(R.string.no_reason_found), getString(R.string.ok), getString(R.string.cancel), "", false)
                } else {
                    val siteAdapter = DamageReasonListAdapter(this@DamageReasonsActivity, unPaidInvoiceMainDO.reasonDOS)
                    recycleview.adapter = siteAdapter


                }
            } else {
                hideLoader()
                showAppCompatAlert("", getString(R.string.no_reason_found), getString(R.string.ok), getString(R.string.cancel), "", false)
            }
        }
        siteListRequest.execute()

//        val driverListRequest = InvoiceHistoryRequest("",this@DamageReasonsActivity)
//        driverListRequest.setOnResultListener { isError, invoiceHistoryMainDO ->
//            hideLoader()
//            if (isError) {
//                tvNoDataFound.setVisibility(View.VISIBLE)
//                recycleview.setVisibility(View.GONE)
//                Toast.makeText(this@DamageReasonsActivity, R.string.error_NoData, Toast.LENGTH_SHORT).show()
//            } else {
//
//                  if(invoiceHistoryMainDO.invoiceHistoryDOS.size>0){
//                      tvNoDataFound.setVisibility(View.GONE)
//                      recycleview.setVisibility(View.VISIBLE)
//                      invoiceAdapter = InvoiceAdapter(this@DamageReasonsActivity, invoiceHistoryMainDO.invoiceHistoryDOS)
//                      recycleview!!.layoutManager = LinearLayoutManager(this@DamageReasonsActivity)
//
//                      recycleview!!.adapter = invoiceAdapter
//                  }else{
//                      tvNoDataFound.setVisibility(View.VISIBLE)
//                      recycleview.setVisibility(View.GONE)
//                  }
//
//
//            }
//        }
//        driverListRequest.execute()

    }

    override fun onBackPressed() {
        var intent = Intent()
        var id = preferenceUtils.getIntFromPreference(PreferenceUtils.DAMAGE_REASON_ID, 0)
        intent.putExtra("data", id)
        setResult(143, intent)
        finish()
        super.onBackPressed()
    }

}