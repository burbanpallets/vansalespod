package com.tbs.generic.vansales.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.ArrayList

/**
 *Created by kishoreganji on 21-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
data class PodDo(
        @SerializedName("id") var id: String? = "",
        @SerializedName("user_id") var user_id: String? = "",
        @SerializedName("starttime") var starttime: String? = "",
        @SerializedName("arrDate") var arrDate: String? = "",
        @SerializedName("arrTime") var arrTime: String? = "",

        @SerializedName("arrivalTime") var arrivalTime: String? = "",
        @SerializedName("unLoadingTime") var unLoadingTime: String? = "",
        @SerializedName("capturedLDTime") var capturedLDTime: String? = "",
        @SerializedName("capturedLRTime") var capturedLRTime: String? = "",

        @SerializedName("capturedProductListTime") var capturedProductListTime: String? = "",
        @SerializedName("cancelRescheduleTime") var cancelRescheduleTime: String? = "",
        @SerializedName("endUnLoadingTime") var endUnLoadingTime: String? = "",
        @SerializedName("paymentsTime") var paymentsTime: String? = "",
        @SerializedName("printsTime") var printsTime: String? = "",
        @SerializedName("capturedReturns") var capturedReturns: String? = "",
        @SerializedName("departureTime") var departureTime: String? = "",
        @SerializedName("notes") var notes: String? = "",
        @SerializedName("notes") var deliveryNotes: String? = "",
        @SerializedName("notes") var invoiceNotes: String? = "",
        @SerializedName("loanDeliverynotes") var loanDeliverynotes: String? = "",
        @SerializedName("loanReturnnotes") var loanReturnnotes: String? = "",
        @SerializedName("loanDeliverysignature") var loanDeliverysignature: String? = "",
        @SerializedName("loanReturnsignature") var loanReturnsignature: String? = "",
        @SerializedName("loanDeliveryname") var loanDeliveryname: String? = "",
        @SerializedName("loanReturnname") var loanReturnname: String? = "",
        @SerializedName("loanDeliveryRating") var loanDeliveryRating: String? = "",
        @SerializedName("loanReturnRating") var loanReturnRating: String? = "",

        @SerializedName("signature") var signature: String? = "",
        @SerializedName("invoice") var invoice: String? = "",
        @SerializedName("payment") var payment: String? = "",
        @SerializedName("name") var name: String? = "",

        @SerializedName("signatureEncode") var signatureEncode: String? = "",
        @SerializedName("capturedImagesList") var capturedImagesList: String? = "",
        @SerializedName("capturedImagesListBulk") var capturedImagesListBulk: ArrayList<String>? = ArrayList(),
        @SerializedName("deliveryStatus") var deliveryStatus: List<String>? = listOf(""),
        @SerializedName("rescheduleStatus") var rescheduleStatus: List<String>? = listOf(""),
        @SerializedName("shipmentList") var shipmentList: ArrayList<ActiveDeliveryDO>? = ArrayList(),
        @SerializedName("capturedLDImagesListBulk") var capturedImagesLDListBulk: ArrayList<String>? = ArrayList(),
        @SerializedName("capturedLRImagesListBulk") var capturedImagesLRListBulk: ArrayList<String>? = ArrayList(),

        //Time Captures
        @SerializedName("podTimeCaptureArrivalTime") var podTimeCaptureArrivalTime: String? = "",
        @SerializedName("podTimeCaptureArrivalDate") var podTimeCaptureArrivalDate: String? = "",
        @SerializedName("podTimeCaptureStartLoadingTime") var podTimeCaptureStartLoadingTime: String? = "",
        @SerializedName("podTimeCaptureStartLoadingDate") var podTimeCaptureStartLoadingDate: String? = "",
        @SerializedName("podTimeCaptureCaptureDeliveryTime") var podTimeCaptureCaptureDeliveryTime: String? = "",
        @SerializedName("podTimeCapturCaptureDeliveryDate") var podTimeCaptureCaptureDeliveryDate: String? = "",
        @SerializedName("podTimeCaptureValidateDeliveryTime") var podTimeCaptureValidateDeliveryTime: String? = "",
        @SerializedName("podTimeCapturValidateDeliveryDate") var podTimeCaptureValidateDeliveryDate: String? = "",
        @SerializedName("podTimeCaptureEndLoadingTime") var podTimeCaptureEndLoadingTime: String? = "",
        @SerializedName("podTimeCaptureEndLoadingDate") var podTimeCaptureEndLoadingDate: String? = "",
        @SerializedName("podTimeCapturepodDepartureTime") var podTimeCapturepodDepartureTime: String? = "",
        @SerializedName("podTimeCapturpodDepartureDate") var podTimeCapturpodDepartureDate: String? = "",


        @SerializedName("podTimeCaptureLDCaptureDeliveryTime") var podTimeCaptureLDCaptureDeliveryTime: String? = "",
        @SerializedName("podTimeCapturLDCaptureDeliveryDate") var podTimeCaptureLDCaptureDeliveryDate: String? = "",
        @SerializedName("podTimeCapturLRCaptureDeliveryDate") var podTimeCaptureLRCaptureDeliveryDate: String? = "",
        @SerializedName("podTimeCaptureLRCaptureDeliveryTime") var podTimeCaptureLRCaptureDeliveryTime: String? = "",
        @SerializedName("podcreDocLattitude") var creDocLattitude: String? = "",
        @SerializedName("podcreDOCLongitude") var creDOCLongitude: String? = "",

        @SerializedName("podcreLDDocLattitude") var creDocLDLattitude: String? = "",
        @SerializedName("podcreLDDOCLongitude") var creDOCLDLongitude: String? = "",

        @SerializedName("podcreLRDocLattitude") var creDocLRLattitude: String? = "",
        @SerializedName("podcreLRDOCLongitude") var creDOCLRLongitude: String? = "",

        @SerializedName("podarrivalLattitude") var arrivalLattitude: String? = "",
        @SerializedName("podarrivalLongitude") var arrivalLongitude: String? = "",
        @SerializedName("poddepartureLattitude") var departureLattitude: String? = "",
        @SerializedName("poddepartureRating") var departureRating: String? = "",

        @SerializedName("poddepartureLongitude") var departureLongitude: String? = ""
) : Serializable