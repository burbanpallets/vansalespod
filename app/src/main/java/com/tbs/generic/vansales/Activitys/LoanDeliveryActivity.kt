package com.tbs.generic.vansales.Activitys

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.*
import com.darsh.multipleimageselect.activities.AlbumSelectActivity
import com.darsh.multipleimageselect.models.Image
import com.tbs.generic.vansales.Adapters.FilesPreviewAdapter
import com.tbs.generic.vansales.Adapters.ScheduledProductsAdapter
import com.tbs.generic.vansales.Model.ActiveDeliveryDO
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO
import com.tbs.generic.vansales.Model.FileDetails
import com.tbs.generic.vansales.Model.PodDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.listeners.StringListner
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.LogUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.include_image_caputure.*
import kotlinx.android.synthetic.main.include_signature.*
import kotlinx.android.synthetic.main.include_signature.btnSignature
import kotlinx.android.synthetic.main.include_signature.imv_signature
import kotlinx.android.synthetic.main.scheduled_capture_delivery.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.util.*
import kotlin.collections.ArrayList


//
class LoanDeliveryActivity : BaseActivity() {


    private var shipmentProductsList: ArrayList<ActiveDeliveryDO>? = ArrayList()
    private var cloneShipmentProductsList: ArrayList<ActiveDeliveryDO> = ArrayList()
    private var type: Int = 1
    lateinit var podDo: PodDo

    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var btnRequestApproval: Button
    lateinit var btnConfirm: Button
    lateinit var tvNoDataFound: TextView
    lateinit var tvApprovalStatus: TextView

    //    lateinit var cbNonBGSelected: CheckBox
    var status = ""
    var customerId = ""

    lateinit var dialog: Dialog
    private var shipmentId: String = ""
    private var loadStockAdapter: ScheduledProductsAdapter? = null
    lateinit var activeDeliverySavedDo: ActiveDeliveryMainDO
    lateinit var fileDetailsDo: ArrayList<FileDetails>

    var nonBgType = 0
    private var signature: String? = null

    private var fileDetailsList: java.util.ArrayList<FileDetails>? = null
    private lateinit var filesPreviewAdapter: FilesPreviewAdapter
    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.scheduled_capture_delivery, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        hideKeyBoard(llBody)
        toolbar.setNavigationOnClickListener {

            if (isSentForApproval) {
                showToast(getString(R.string.please_process_order))
            } else {
                LogUtils.debug("PAUSE", "tes")
                var data = tvApprovalStatus.text.toString()
                var intent = Intent()
                intent.putExtra("Status", data)
                var args = Bundle()
                args.putSerializable("List", shipmentProductsList)
                intent.putExtra("BUNDLE", args)
                setResult(56, intent)
                finish()
            }
        }

        initializeControls()
        activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        podDo = StorageManager.getInstance(this).getDepartureData(this);
        fileDetailsDo = StorageManager.getInstance(this).getLoanDeliveryImagesData(this)

        if (podDo.loanDeliverysignature!!.isNotEmpty()) {
            val decodedString = android.util.Base64.decode(podDo.loanDeliverysignature, android.util.Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            imv_signature.visibility = VISIBLE
            imv_signature?.setImageBitmap(decodedByte)
        }

        btnSignature.setOnClickListener {
            val intent = Intent(this, CommonSignatureActivity::class.java)
            intent.putExtra("LD",1)
            startActivityForResult(intent, 9907)
        }
        imagesSetUp()
        btnAddImages.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(this, AlbumSelectActivity::class.java)
            intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 1)
            if (fileDetailsList!!.size > 0 && fileDetailsList!!.size <= 4) {
                intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 4 - fileDetailsList!!.size)
            } else {
                intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 4)
            }
            intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_FROM, true)
            startActivityForResult(intent, 888)
        }

        shipmentId = activeDeliverySavedDo.loandelivery


        if (intent.hasExtra("CustomerId")) {
            customerId = intent.extras?.getString("CustomerId")!!
        }

        tvScreenTitle.text = getString(R.string.confirm_products)


        if (podDo!!.deliveryStatus?.get(0)!!.contains("Requested", true)) {
            tvApprovalStatus.visibility = VISIBLE
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnConfirm.isClickable = false
            btnConfirm.isEnabled = false
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnRequestApproval.isClickable = false
            btnRequestApproval.isEnabled = false
            tvApprovalStatus.text = podDo!!.deliveryStatus?.get(0)
        } else if (podDo!!.deliveryStatus?.get(0)!!.contains("Approved", true)) {
            tvApprovalStatus.visibility = VISIBLE

            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnRequestApproval.isClickable = false
            btnRequestApproval.isEnabled = false
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
            btnConfirm.isClickable = true
            btnConfirm.isEnabled = true
            tvApprovalStatus.text = podDo!!.deliveryStatus?.get(0)

        } else {
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
            btnRequestApproval.isClickable = true
            btnRequestApproval.isEnabled = true
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
            btnConfirm.isClickable = true
            btnConfirm.isEnabled = true
            tvApprovalStatus.text = podDo!!.deliveryStatus?.get(0)
        }



        btnConfirm.text = getString(R.string.confirm)
        if (activeDeliverySavedDo.shipmentNumber.contains("SDH")) {
            ivAdd.visibility = GONE

        } else {
            ivAdd.visibility = VISIBLE

        }

        if (tvApprovalStatus.text.toString().contains("Requested")) {
            btnConfirm.text = getString(R.string.confirm)
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnConfirm.isClickable = false
            btnConfirm.isEnabled = false
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnRequestApproval.isClickable = false
            btnRequestApproval.isEnabled = false
        } else {
            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                btnConfirm.text = getString(R.string.confirm)
                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirm.isClickable = false
                btnConfirm.isEnabled = false
//                    cbNonBGSelected.setVisibility(GONE)
                btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnRequestApproval.isClickable = false
                btnRequestApproval.isEnabled = false
            } else {
                btnConfirm.text = getString(R.string.confirm)
                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                btnConfirm.isClickable = true
                btnConfirm.isEnabled = true
//                    cbNonBGSelected.setVisibility(VISIBLE)

                btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnRequestApproval.isClickable = false
                btnRequestApproval.isEnabled = false
            }

        }
        loadScheduleData()


        llnoteFab.setOnClickListener {
            var intent = Intent(this@LoanDeliveryActivity, PODNotesActivity::class.java);
            intent.putExtra("TYPE",1)
            startActivityForResult(intent, 140);
        }
        ivAdd.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(this, TrailerEquipmentsActivity::class.java)
            intent.putExtra("FROM", "SCHEDULED")
            startActivityForResult(intent, 1)

        }

        btnRequestApproval.setOnClickListener {
            Util.preventTwoClick(it)
            requestApproval()
        }

        btnConfirm.setOnClickListener {
            Util.preventTwoClick(it)
            if (signature.isNullOrEmpty() && podDo.loanDeliverysignature.isNullOrEmpty()) {
                showToast(resources.getString(R.string.please_do_sig))
            } else {
                if (!lattitudeFused.isNullOrEmpty()) {
                    stopLocationUpdates()
                    hideLoader()
                    if (isProductsChanged(shipmentProductsList!!)) {
                        modify()
                    } else {
                        modify()
                    }
                } else {
                    showLoader()
                    stopLocationUpdates()
                    location(ResultListner { `object`, isSuccess ->
                        if (isSuccess) {
                            hideLoader()
                            stopLocationUpdates()
                            if (isProductsChanged(shipmentProductsList!!)) {
                                modify()

                            } else {
                                modify()
                            }

                        }
                    })
                }


            }


        }
    }

    private fun validatedelivery() {
        setUpImagesAs64Bit()
        val driverListRequest = ValidateDeliveryRequest(podDo!!.capturedImagesLDListBulk, signature, podDo!!.deliveryNotes, shipmentId,
                lattitudeFused, longitudeFused, addressFused, this@LoanDeliveryActivity)
        driverListRequest.setOnResultListener { isError, validateDo, msg ->
            hideLoader()
            if (isError) {
                hideLoader()
                if (msg.isNotEmpty()) {
                    showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.failure), false)
                } else {
                    showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)
                }
            } else {

                if (validateDo.status == 20) {
                    preferenceUtils.saveInt(PreferenceUtils.VALIDATED_FLAG, 2)
                    hideLoader()

                    if (validateDo.message.length > 0) {
                        showToast("" + validateDo.message)

                    } else {

                        Toast.makeText(this@LoanDeliveryActivity, getString(R.string.shipment_validated), Toast.LENGTH_SHORT).show()
                    }
                    podDo!!.podTimeCaptureLDCaptureDeliveryTime = CalendarUtils.getTime()
                    podDo!!.podTimeCaptureLDCaptureDeliveryDate = CalendarUtils.getDate()
                    StorageManager.getInstance(this).saveDepartureData(this, podDo)
                    StorageManager.getInstance(this).deleteLoanDeliveryImagesData(this)
                    val intent = Intent()
                    intent.putExtra("CRELAT", lattitudeFused)
                    intent.putExtra("CRELON", longitudeFused)
                    intent.putExtra("RATING", podDo!!.loanDeliveryRating)
                    intent.putExtra("NOTES", podDo!!.loanDeliverynotes)
                    intent.putExtra("NAME", podDo!!.loanDeliveryname)
                    setResult(105, intent)
                    finish()
                } else {
                    if (validateDo.message.length > 0) {
                        showToast("" + validateDo.message)
                    } else {
                        showToast(getString(R.string.shipment_not_validate))

                    }
                    preferenceUtils.saveInt(PreferenceUtils.VALIDATED_FLAG, 1)

                }

            }
        }
        driverListRequest.execute()


    }


    private fun imagesSetUp() {

        fileDetailsList = fileDetailsDo
        filesPreviewAdapter = FilesPreviewAdapter(this, fileDetailsList, StringListner {

            if (fileDetailsList!!.size < 4) {
                btnAddImages.visibility = VISIBLE

            } else {
                btnAddImages.visibility = GONE
            }

            if (fileDetailsList!!.size >= 1) {
                recycleviewImages.visibility = VISIBLE
            } else {
                recycleviewImages.visibility = GONE
            }
        })
        recycleviewImages.adapter = filesPreviewAdapter
        if (fileDetailsList!!.size > 0) {
            recycleviewImages.visibility = VISIBLE

        }
    }


    private fun updateTheProductsInLocal() {
        try {
            if (loadStockAdapter != null && loadStockAdapter!!.activeDeliveryDOS != null && loadStockAdapter!!.activeDeliveryDOS.size > 0) {

                val deliveryDos = loadStockAdapter!!.activeDeliveryDOS
                StorageManager.getInstance(this@LoanDeliveryActivity).saveCurrentDeliveryItems(this, deliveryDos)
                val updatedRows = StorageManager.getInstance(this).updateTheProductsInLocal(loadStockAdapter!!.activeDeliveryDOS)
//                if (updatedRows > 0) {
//                val intent = Intent()
//                intent.putExtra("DeliveredProducts", loadStockAdapter!!.activeDeliveryDOS)
//                intent.putExtra("Status", tvApprovalStatus.text.toString().trim())
//                setResult(1, intent)
//                finish()
//                } else {
//                    showToast("not confirmed")
//                }
            } else {
                showToast(getString(R.string.there_are_no_products_to_confirm))
            }
        } catch (e: Exception) {
        }
    }

    private fun loadScheduleData() {

        if (Util.isNetworkAvailable(this)) {
            if (shipmentId.length > 0) {

                val driverListRequest = ActiveDeliveryRequest(1, shipmentId, this)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {
                        Log.e("loadScheduleData", "Error at loading loadScheduleData()")
                    } else {
//                        StorageManager.getInstance(this).saveActiveDeliveryMainDo(this, activeDeliveryDo)
                        shipmentProductsList = activeDeliveryDo.activeDeliveryDOS
                        if (shipmentProductsList != null && shipmentProductsList!!.size > 0) {
                            cloneShipmentProductsList.addAll(shipmentProductsList!!)
                            loadStockAdapter = ScheduledProductsAdapter(this, shipmentProductsList, "")
                            recycleview.adapter = loadStockAdapter
                            recycleview.visibility = VISIBLE
                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
//                                cbNonBGSelected.visibility = GONE

                            } else {
//                                cbNonBGSelected.visibility = VISIBLE

                            }

                            tvNoDataFound.visibility = GONE
                            tvApprovalStatus.visibility = View.VISIBLE
//                            btnRequestApproval.visibility = View.VISIBLE
                            btnConfirm.visibility = View.VISIBLE

                        } else {
                            tvApprovalStatus.visibility = View.GONE
                            btnRequestApproval.visibility = View.GONE
                            btnConfirm.visibility = View.GONE
                            recycleview.visibility = GONE
                            tvNoDataFound.visibility = VISIBLE
                            btnConfirm.isEnabled = false
                            btnConfirm.isClickable = false
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        }
                    }
                }
                driverListRequest.execute()


            }
        }


//        }
    }

    fun deleteShipmentProducts(loadStockDO: ActiveDeliveryDO) {
        this.loadStockDO = loadStockDO
        showAppCompatAlert("", getString(R.string.do_you_want_delete_this_pr), getString(R.string.delete), getString(R.string.cancel), "DeleteProduct", false)
    }

    lateinit var loadStockDO: ActiveDeliveryDO

    override fun initializeControls() {
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        btnRequestApproval = findViewById<View>(R.id.btnRequestApproval) as Button
        btnConfirm = findViewById<View>(R.id.btnConfirm) as Button
        tvNoDataFound = findViewById<View>(R.id.tvNoDataFound) as TextView
        tvApprovalStatus = findViewById<TextView>(R.id.tvApprovalStatus)
        var approval = tvApprovalStatus.text.toString()
        status = preferenceUtils.getStringFromPreference(PreferenceUtils.PRODUCT_APPROVAL, "")
        if (approval.equals("NA")) {
            tvApprovalStatus.text = getString(R.string.na)

        } else {
            tvApprovalStatus.text = status

        }

        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        ivRefresh.setOnClickListener {
            Util.preventTwoClick(it)
            if (tvApprovalStatus.text.toString().contains("Request")) {
                preferenceUtils.saveString(PreferenceUtils.PRODUCT_APPROVAL, tvApprovalStatus.text.toString())
                updateStatus()
            }
        }
    }

    private fun requestApproval() {
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = ProductsModifyRequest(4, shipmentId, shipmentProductsList, this@LoanDeliveryActivity)
            siteListRequest.setOnResultListener { isError, approveDO, msg ->
                hideLoader()
                if (isError) {
                    showAlert(getString(R.string.server_erro))
//                        Log.e("RequestAproval", "Response : " + isError)
                } else {
                    btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnRequestApproval.isClickable = false
                    btnRequestApproval.isEnabled = false
                    tvApprovalStatus.visibility = VISIBLE
                    if (approveDO.flag == 4) {
                        tvApprovalStatus.text = getString(R.string.statue_requested)
                        isSentForApproval = true
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false
                    } else {
                        Toast.makeText(this@LoanDeliveryActivity, getString(R.string.please_send_request_again), Toast.LENGTH_SHORT).show()
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnRequestApproval.isClickable = true
                        btnRequestApproval.isEnabled = true
                        tvApprovalStatus.text = getString(R.string.statues_request_not_sent)
                    }

                }

            }
            siteListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

        }


    }

    private fun modify() {

        loadData()


    }

    private var isSentForApproval: Boolean = false
    private fun updateStatus() {
        val siteListRequest = CancelRescheduleApprovalStatusRequest(shipmentId, this)
        siteListRequest.setOnResultListener { isError, approvalDO ->
            hideLoader()
            if (approvalDO != null) {
                if (isError) {
                    Toast.makeText(this@LoanDeliveryActivity, getString(R.string.approval_under_process), Toast.LENGTH_SHORT).show()
                } else {
//                    approvalDO.status = "Approved"
                    if (approvalDO.status.equals("Approved", true)) {
                        loadDeliveryData()
                        isSentForApproval = true
                        tvApprovalStatus.text = getString(R.string.status) + " : " + approvalDO.status
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false


                    } else if (approvalDO.status.equals("Rejected", true)) {
                        rejectProducts()
                        isSentForApproval = true
                        tvApprovalStatus.text = getString(R.string.status) + " : " + approvalDO.status
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false

                    } else {
                        Toast.makeText(this@LoanDeliveryActivity, getString(R.string.approval_under_process), Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        siteListRequest.execute()
    }

    private fun isProductsChanged(shipmentProductsList: ArrayList<ActiveDeliveryDO>): Boolean {
        if (cloneShipmentProductsList != null && shipmentProductsList != null) {
            if (shipmentProductsList.size != cloneShipmentProductsList.size) {
                return true
            } else if (shipmentProductsList.size == cloneShipmentProductsList.size) {
                for (i in cloneShipmentProductsList.indices) {
                    if (shipmentProductsList.get(i).orderedQuantity != cloneShipmentProductsList.get(i).totalQuantity) {
                        return true
                    }
                }
            } else {
                return false
            }
        }
        return false
    }

    override fun onBackPressed() {

        if (isSentForApproval) {
            showToast(getString(R.string.please_process_order))
        } else {
            LogUtils.debug("PAUSE", "tes")
            var data = tvApprovalStatus.text.toString()
            var intent = Intent()
            intent.putExtra("Status", data)
            intent.putExtra("List", shipmentProductsList)
            setResult(56, intent)
            finish()
            super.onBackPressed()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
            var from = data!!.getStringExtra("FROM")
            val activeDeliveryDOS = data.getSerializableExtra("AddedProducts") as ArrayList<ActiveDeliveryDO>
            if (activeDeliveryDOS.size > 0) {
                recycleview.visibility = VISIBLE
                tvNoDataFound.visibility = GONE

                if (loadStockAdapter == null) {
                    loadStockAdapter = ScheduledProductsAdapter(this, activeDeliveryDOS, "")
                    recycleview.adapter = loadStockAdapter
                } else {
                    var isProductExisted = false
                    for (i in activeDeliveryDOS.indices) {
                        for (k in shipmentProductsList!!.indices) {
                            if (!shipmentProductsList!!.get(k).pType.equals("SCHEDULED") && activeDeliveryDOS.get(i).product.equals(shipmentProductsList!!.get(k).product, true)
                                    && activeDeliveryDOS.get(i).shipmentId.equals(shipmentProductsList!!.get(k).shipmentId, true)) {
                                shipmentProductsList!!.get(k).orderedQuantity = activeDeliveryDOS.get(i).orderedQuantity
                                isProductExisted = true
                                break
                            }
                        }
                        if (isProductExisted) {
                            isProductExisted = false
                            continue
                        } else {
                            shipmentProductsList!!.add(activeDeliveryDOS.get(i))
                        }
                    }
                    loadStockAdapter!!.refreshAdapter(shipmentProductsList)
                }

            }
        }
        if (resultCode == RESULT_OK && requestCode == 888) {
            val images = data?.getParcelableArrayListExtra<Image>(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_IMAGES)

            if (images != null) {

                var i = 0
                val l = images.size
                while (i < l) {
                    val file1 = File(images[i].path)
                    val fileDetails = FileDetails()
                    fileDetails.fileName = file1.name
                    fileDetails.filePath = file1.absolutePath
                    fileDetailsList?.add(0, fileDetails)
                    StorageManager.getInstance(this).saveLoanDeliveryImagesData(this, fileDetailsList)
                    i++
                }
                filesPreviewAdapter.notifyDataSetChanged()

                if (fileDetailsList!!.size < 4) {
                    btnAddImages.visibility = View.VISIBLE
                } else {
                    btnAddImages.visibility = View.GONE
                }

                if (fileDetailsList!!.size >= 1) {
                    recycleviewImages.visibility = View.VISIBLE
                } else {
                    recycleviewImages.visibility = View.GONE
                }
            }
        }
        if (resultCode == RESULT_OK && requestCode == 9907) {
            signature = data!!.getStringExtra("Signature")
            Log.d("Signature----->", signature)
            var rating = data!!.getStringExtra("Rating")

            podDo!!.loanDeliveryRating = rating
            podDo!!.loanDeliverysignature = signature
            var name = data!!.getStringExtra("Name")

            podDo!!.loanDeliveryname = name

            StorageManager.getInstance(this).saveDepartureData(this@LoanDeliveryActivity, podDo)
            val decodedString = android.util.Base64.decode(signature, android.util.Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            imv_signature.visibility = VISIBLE
            imv_signature?.setImageBitmap(decodedByte)

        }
        if (requestCode == 140 && resultCode == Activity.RESULT_OK) {
            val returnString = data!!.getStringExtra(resources.getString(R.string.notes_remarks))
            LogUtils.debug(resources.getString(R.string.notes_remarks), returnString)
            podDo!!.loanDeliverynotes = returnString;
            StorageManager.getInstance(this).saveDepartureData(this@LoanDeliveryActivity, podDo)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        hideLoader()
    }

    private fun rejectProducts() {

        loadStockAdapter = ScheduledProductsAdapter(this, activeDeliverySavedDo.activeDeliveryDOS, "")
        recycleview.adapter = loadStockAdapter
    }

    private fun loadDeliveryData() {

        if (Util.isNetworkAvailable(this)) {
            if (shipmentId.length > 0) {
                cloneShipmentProductsList.removeAll(shipmentProductsList!!)
                val driverListRequest = ActiveDeliveryRequest(1, shipmentId, this)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {
                        Log.e("loadScheduleData", "Error at loading loadScheduleData()")
                    } else {
//                        StorageManager.getInstance(this).saveActiveDeliveryMainDo(this, activeDeliveryDo)
                        shipmentProductsList = activeDeliveryDo.activeDeliveryDOS
                        if (shipmentProductsList != null && shipmentProductsList!!.size > 0) {
                            cloneShipmentProductsList.addAll(shipmentProductsList!!)
                            loadStockAdapter = ScheduledProductsAdapter(this, shipmentProductsList, "")
                            recycleview.adapter = loadStockAdapter
                            recycleview.visibility = VISIBLE
//                            cbNonBGSelected.visibility = VISIBLE
                            tvNoDataFound.visibility = GONE
                            tvApprovalStatus.visibility = View.VISIBLE
//                            btnRequestApproval.visibility = View.VISIBLE
                            btnConfirm.visibility = View.VISIBLE
                        } else {
                            tvApprovalStatus.visibility = View.GONE
                            btnRequestApproval.visibility = View.GONE
                            btnConfirm.visibility = View.GONE
                            recycleview.visibility = GONE
                            tvNoDataFound.visibility = VISIBLE
                            btnConfirm.isEnabled = false
                            btnConfirm.isClickable = false
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        }
                    }
                }
                driverListRequest.execute()


            }
        }


//        }
    }

    private fun loadData() {

        if (Util.isNetworkAvailable(this)) {
            if (shipmentId.length > 0) {
                val driverListRequest = ActiveDeliveryRequest(1, shipmentId, this)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {
                        Log.e("loadScheduleData", "Error at loading loadScheduleData()")
                    } else {

//                        StorageManager.getInstance(this).saveActiveDeliveryMainDo(this, activeDeliveryDo)
                        var isUpdated = false
                        var pickUpDos = activeDeliveryDo.activeDeliveryDOS
                        for (k in pickUpDos.indices) {
                            if (pickUpDos.get(k).serialupdate.isNullOrEmpty()) {
                                isUpdated = false
                                break
                            } else {
                                isUpdated = true
                            }
                        }
                        if (isUpdated == true||activeDeliveryDo.validatedFlag==2) {
                            if(activeDeliveryDo.validatedFlag==2){
                                validatedelivery()
                            }else{
                                if (Util.isNetworkAvailable(this)) {
                                    val siteListRequest = DeliverysModifyRequest(shipmentId, activeDeliveryDo.site, shipmentProductsList, this@LoanDeliveryActivity)
                                    siteListRequest.setOnResultListener { isError, approveDO, msg ->
                                        hideLoader()
                                        if (isError) {
                                            if (msg.isNullOrEmpty()) {
                                                showAlert(getString(R.string.server_error))

                                            } else {
                                                showAlert(msg)

                                            }

                                        } else {
                                            if (approveDO.flag == 2) {
                                                validatedelivery()

                                            } else {
                                                if (msg.isNullOrEmpty()) {
                                                    showAlert(getString(R.string.server_error))

                                                } else {
                                                    showAlert(msg)

                                                }

                                            }


                                        }

                                    }
                                    siteListRequest.execute()
                                } else {
                                    showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

                                }
                            }

                        } else {
                            showAlert(getString(R.string.updateserials))
                        }

                    }
                }
                driverListRequest.execute()


            }
        }


//        }
    }

    private fun setUpImagesAs64Bit() {
        podDo!!.capturedImagesLDListBulk = ArrayList()
        for (list in this.fileDetailsList!!) {
            try {
                val fis = FileInputStream(File(list.filePath))
                val bitmap = BitmapFactory.decodeStream(fis)
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val bytes = baos.toByteArray()
                val encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                LogUtils.debug("PIC-->", encImage)
                podDo!!.capturedImagesLDListBulk!!.add(encImage)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        StorageManager.getInstance(this).saveDepartureData(this@LoanDeliveryActivity, podDo)

    }

}