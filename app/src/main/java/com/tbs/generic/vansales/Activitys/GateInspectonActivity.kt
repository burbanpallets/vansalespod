//package com.tbs.generic.vansales.Activitys
//
//import androidx.recyclerview.widget.LinearLayoutManager
//import androidx.recyclerview.widget.RecyclerView
//import android.view.View
//import android.view.ViewGroup
//import android.widget.*
//import com.tbs.generic.vansales.Model.InspectionDO
//import com.tbs.generic.vansales.R
//import com.tbs.generic.vansales.Requests.GateInspectionRequest
//import com.tbs.generic.vansales.database.StorageManager
//import com.tbs.generic.vansales.utils.Util
//import java.util.ArrayList
//
//
//class GateInspectonActivity : BaseActivity() {
//
//    lateinit var btnSkip:Button
//    lateinit var btnCompleted:Button
//    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
//    lateinit var tvNoDataFound: TextView
//    override fun onResume() {
//        super.onResume()
//    }
//    override fun initialize() {
//        var  llCategories = layoutInflater.inflate(R.layout.gate_inspection, null) as RelativeLayout
//        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
//        changeLocale()
//        toolbar.setNavigationIcon(R.drawable.back)
//        toolbar.setNavigationOnClickListener { finish() }
//        disableMenuWithBackButton()
//        initializeControls()
//        btnCompleted.isEnabled = false
//        btnCompleted.isClickable= false
//        btnCompleted.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//        val driverListRequest = GateInspectionRequest(this@GateInspectonActivity)
//        driverListRequest.setOnResultListener { isError, inspectionMainDo ->
//
//            if (isError) {
//                hideLoader()
//                recycleview.visibility = View.GONE
//                tvNoDataFound.visibility = View.VISIBLE
//                Toast.makeText(this@GateInspectonActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
//            } else {
//               hideLoader()
//                recycleview.visibility = View.VISIBLE
//                tvNoDataFound.visibility = View.GONE
//                var inspectionAdapter = InspectionAdapter(this@GateInspectonActivity, inspectionMainDo.inspectionDOS)
//                recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@GateInspectonActivity)
//                StorageManager.getInstance(this).saveGateInspectionList(this, inspectionMainDo.inspectionDOS)
//                recycleview.adapter = inspectionAdapter
//
//            }
//        }
//        showLoader()
//        driverListRequest.execute()
//        btnSkip.setOnClickListener{
//            finish()
//        }
//        btnCompleted.setOnClickListener {
//            Util.preventTwoClick(it)
//            setResult(6, null)
//            finish()
//        }
//    }
//    override fun initializeControls() {
//        tvScreenTitle.setText(R.string.gate_inspection)
//        //ivBack.visibility = View.GONE
//
//        btnSkip             = findViewById<Button>(R.id.btnSkip)
//        btnCompleted        = findViewById<Button>(R.id.btnCompleted)
//        ivMenu.visibility = View.VISIBLE
//        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
//        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
//        tvNoDataFound = findViewById<TextView>(R.id.tvNoDataFound)
//
//        recycleview.layoutManager = linearLayoutManager
//    }
//
//    fun enableDisableComplete(inspectionDOs: ArrayList<InspectionDO>) {
//        var icChanged = false
//        val inspectionDoS = StorageManager.getInstance(this).getGateInspectionList(this)
//        if(inspectionDoS!=null && inspectionDoS.size>0){
//            for (i in inspectionDoS.indices){
//                if(inspectionDoS.get(i).isSelected != inspectionDOs.get(i).isSelected){
//                    icChanged = true
//                    break
//                }
//            }
//            if(icChanged){
//                btnCompleted.isEnabled = true
//                btnCompleted.isClickable= true
//                btnCompleted.setBackgroundColor(resources.getColor(R.color.md_green))
//            }
//            else{
//                btnCompleted.isEnabled = false
//                btnCompleted.isClickable= false
//                btnCompleted.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//            }
//        }
//    }
//}