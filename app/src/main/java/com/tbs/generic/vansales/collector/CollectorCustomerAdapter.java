package com.tbs.generic.vansales.collector;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class CollectorCustomerAdapter extends RecyclerView.Adapter<CollectorCustomerAdapter.MyViewHolder> {

    private ArrayList<CustomerDo> customerDos;
    private Context context;
    private String from;

    private ArrayList<CustomerDo> selectedCustomerDOs = new ArrayList<>();
    private ArrayList<Integer> selectedCustomerPos = new ArrayList<>();
    private SparseBooleanArray itemStateArray = new SparseBooleanArray();

    public void refreshAdapter(@NotNull ArrayList<CustomerDo> customerDos) {
        this.customerDos = customerDos;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvCustomerName, tvCustomerCode, tvBuisinessLine, tvAddressCode, tvDescription, tvCity, tvEmirates;
        public TextView btnPay;
        private LinearLayout llDetails;
        public TextView tvOrderId;
        private CheckBox cbSelected;

        public MyViewHolder(View view) {
            super(view);
            llDetails = itemView.findViewById(R.id.llDetails);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvCustomerCode = itemView.findViewById(R.id.tvCustomerCode);
            tvBuisinessLine = itemView.findViewById(R.id.tvBuisinessLine);
            tvAddressCode = itemView.findViewById(R.id.tvAddressCode);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvCity = itemView.findViewById(R.id.tvCity);
            tvEmirates = itemView.findViewById(R.id.tvEmirates);
            cbSelected = itemView.findViewById(R.id.cbSelected);
//            itemView.setOnClickListener(this);
//
//            this.setIsRecyclable(false);

        }

        void bind(int position) {
            // use the sparse boolean array to check
            if (!itemStateArray.get(position, false)) {
                cbSelected.setChecked(false);
            } else {
                cbSelected.setChecked(true);
            }
        }

        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            if (!itemStateArray.get(adapterPosition, false)) {
                cbSelected.setChecked(true);
                itemStateArray.put(adapterPosition, true);
            } else {
                cbSelected.setChecked(false);
                itemStateArray.put(adapterPosition, false);
            }
        }
    }


    public CollectorCustomerAdapter(Context context, ArrayList<CustomerDo> listOrderDos, String froM) {
        this.context = context;
        this.customerDos = listOrderDos;
        this.from = froM;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.collector_customer_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        if ((position % 2 == 0)) {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_grey));
        } else {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_secnd_grey));
        }

        final CustomerDo customerDo = customerDos.get(position);
//        holder.bind(position);
//        holder.tvCustomerName.setText("Name                : " + customerDo.customerName +
//                "\n" + "Business line   : " + customerDo.businessLine+
//                "\n" + "Address code  : " + customerDo.postBox+
//                "\n" + "Description      : " + customerDo.landmark+
//                "\n" + "City                   : " + customerDo.city+
//                "\n" + "Emirates          : " + customerDo.emirates
//        );
        holder.tvCustomerCode.setText(customerDo.customer);
        holder.tvCustomerName.setText(customerDo.customerName);
        holder.tvBuisinessLine.setText(customerDo.businessLine);
        holder.tvAddressCode.setText(customerDo.postBox);
        holder.tvDescription.setText(customerDo.landmark);
        holder.tvCity.setText(customerDo.city);
        holder.tvEmirates.setText(customerDo.emirates);

//        if (customerDo.postBox.length() > 0 || customerDo.landmark.length() > 0 || customerDo.town.length() > 0) {
//            holder.tvCustomerAddress.setVisibility(View.VISIBLE);
//
//            holder.tvCustomerAddress.setText("Address            : " + customerDo.postBox + "  \n" + customerDo.landmark + customerDo.town);
//        } else {
//            holder.tvCustomerAddress.setVisibility(View.GONE);
//        }
        holder.cbSelected.setOnCheckedChangeListener(null);
        if (selectedCustomerDOs.contains(customerDo)) {
            holder.cbSelected.setChecked(true);
        } else {
            holder.cbSelected.setChecked(false);
        }

        holder.cbSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedCustomerDOs.add(customerDo);
//                    selectedCustomerPos.add(Integer.valueOf(position));
                } else {
                    selectedCustomerDOs.remove(customerDo);
//                    selectedCustomerPos.remove(Integer.valueOf(position));
                }
            }
        });
        if (customerDo.isDelivered != null && customerDo.isDelivered.equalsIgnoreCase("Delivered")) {
            holder.llDetails.setBackgroundColor(context.getResources().getColor(R.color.gray_new));
            holder.llDetails.setClickable(false);
            holder.llDetails.setEnabled(false);
        } else {

            holder.llDetails.setClickable(true);
            holder.llDetails.setEnabled(true);
        }
        holder.llDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(context, InvoiceListActivity.class);
//                intent.putExtra("CODE",customerDo.customer);
//
//                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
//        if (filterList != null && filterList.size() > 0) {
//            return filterList.size();
//
//        } else {
//        }
        return customerDos != null ? customerDos.size() : 0;

    }

    public ArrayList<CustomerDo> getselectedCustomerDOs() {

        return selectedCustomerDOs;
    }
}
