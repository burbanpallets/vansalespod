//package com.tbs.generic.vansales.Adapters;
//
///**
// * Created by sandy on 2/7/2018.
// */
//
//import android.content.Context;
//import androidx.recyclerview.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.RadioButton;
//import android.widget.RadioGroup;
//import android.widget.TextView;
//
//import com.tbs.generic.vansales.Activitys.GateInspectonActivity;
//import com.tbs.generic.vansales.Model.InspectionDO;
//import com.tbs.generic.vansales.R;
//
//import java.util.ArrayList;
//
//public class InspectionAdapter extends RecyclerView.Adapter<InspectionAdapter.MyViewHolder>  {
//
//    private ArrayList<InspectionDO> inspectionDOS;
//    private Context context;
//
//
//
//
//
//    public class MyViewHolder extends RecyclerView.ViewHolder {
//        public TextView tvQuestion;
//        private RadioGroup rgQuestions;
//        private RadioButton rbNo, rbYes;
//        private LinearLayout llDetails;
//
//        public MyViewHolder(View view) {
//            super(view);
//            llDetails   = view.findViewById(R.id.llDetails);
//            tvQuestion  = view.findViewById(R.id.tvQuestion);
//            rgQuestions = view.findViewById(R.id.rgQuestions);
//            rbNo        = view.findViewById(R.id.rbNo);
//            rbYes       = view.findViewById(R.id.rbYes);
//        }
//    }
//
//
//    public InspectionAdapter(Context context, ArrayList<InspectionDO> inspectionDoS) {
//        this.context = context;
//        this.inspectionDOS = inspectionDoS;
//    }
//
//    @Override
//    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.inspection_data, parent, false);
//        return new MyViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(final MyViewHolder holder, final int position) {
//
//        final InspectionDO inspectionDO = inspectionDOS.get(position);
//        holder.tvQuestion.setText(inspectionDO.question);
//
//        if(inspectionDO.isSelected == position){
//            holder.rbYes.setChecked(true);
//            holder.rbNo.setChecked(false);
//        }
//        else {
//            holder.rbNo.setChecked(false);
//            holder.rbYes.setChecked(true);
//        }
//        holder.rbNo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(holder.rbNo.isChecked()){
//                    inspectionDO.isSelected = position;
//                    holder.rbNo.setTag(position);
//                }
//                else {
//                    holder.rbNo.setTag(-1);
//                    inspectionDO.isSelected = -1;
//                }
//                if(context instanceof VehicleInspectionActivity){
//                    ((VehicleInspectionActivity)context).enableDisableComplete(inspectionDOS);
//                }
//                else if(context instanceof GateInspectonActivity){
//                    ((GateInspectonActivity)context).enableDisableComplete(inspectionDOS);
//                }
//            }
//        });
//        holder.rbYes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(holder.rbYes.isChecked()){
//                    inspectionDO.isSelected = -1;
//                    holder.rbYes.setTag(-1);
//                }
//                else {
//                    holder.rbYes.setTag(-1);
//                }
//                if(context instanceof VehicleInspectionActivity){
//                    ((VehicleInspectionActivity)context).enableDisableComplete(inspectionDOS);
//                }
//                else if(context instanceof GateInspectonActivity){
//                    ((GateInspectonActivity)context).enableDisableComplete(inspectionDOS);
//                }
//            }
//        });
//    }
//
//    @Override
//    public int getItemCount() {
//        return inspectionDOS.size();
//    }
//}