package com.tbs.generic.vansales.here;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.location.LocationManager;
import android.opengl.GLES20;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.LocationDataSourceHERE;
import com.here.android.mpa.common.MapSettings;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.guidance.NavigationManager;
import com.here.android.mpa.guidance.TrafficUpdater;
import com.here.android.mpa.guidance.TruckRestrictionNotification;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.mapping.MapState;
import com.here.android.mpa.mapping.OnMapRenderListener;
import com.here.android.mpa.odml.MapLoader;
import com.here.android.mpa.odml.MapPackage;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RouteTta;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.routing.Router;
import com.here.android.mpa.routing.RoutingError;
import com.here.android.mpa.routing.TransitManeuver;
import com.here.android.positioning.StatusListener;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.R;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

public class HereMapActivity extends BaseActivity {

    private static final String TAG = "HereMapActivity";
    private Button btnStart;
    private ImageView ivCar, ivTruck, ivWalk;
    private TextView tvTime,tvDTime,tvDDistance, tvDistance, tvDirection, tvReCenter;
    private LinearLayout llInstructions, llNavigation, llBottom;

    private DecimalFormat decimalFormat = new DecimalFormat("0.00");
    private AndroidXMapFragment m_mapFragment;
    private TrafficUpdater.RequestInfo m_requestInfo;
    private Map map;
    private Route m_route;
    private MapRoute mapRoute;
    private boolean mTransforming;
    private NavigationManager m_navigationManager;
    private PositioningManager mPositioningManager;
    private LocationDataSourceHERE mHereLocation;
    private GeoCoordinate currentLocGeoCordinate, destLocGeoCordinate;
    private Runnable mPendingUpdate;
    private String mapScheme = Map.Scheme.NORMAL_DAY;
    private GeoBoundingBox m_geoBoundingBox;

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission)
                        != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (hasPermissions(this, Constants.RUNTIME_PERMISSIONS)) {
            initializeMapsAndPositioning();
        } else {
            ActivityCompat.requestPermissions(this, Constants.RUNTIME_PERMISSIONS, Constants.REQUEST_CODE_ASK_PERMISSIONS);
        }
    }

    private boolean isGpsEnabled(){
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
//        boolean network_enabled = false;
        try {
            return gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }
//        catch(Exception ex) {}
//        try {
//            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
//        }
        catch(Exception ex) {}
//        if(!gps_enabled && !network_enabled) {
//            // notify user
//            new AlertDialog.Builder(context)
//                    .setMessage(R.string.gps_network_not_enabled)
//                    .setPositiveButton(R.string.open_location_settings, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
//                            context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//                        }
//                    }
//                            .setNegativeButton(R.string.Cancel,null)
//                            .show();
//        }
        return false;
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        if(!isGpsEnabled()){
//            new AlertDialog.Builder(HereMapActivity.this)
//                    .setCancelable(false)
//                    .setMessage("Please enable GPS")
//                    .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
//                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//                        }
//                    }).setNegativeButton("Cancel",null).show();
//        }
//    }

    private void initializeMapsAndPositioning(){
        setContentView(R.layout.here_map_layout);
        initializeControls();
        m_mapFragment               = (AndroidXMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);
        m_mapFragment.setRetainInstance(true);
        try {
            String diskCacheRoot = getFilesDir().getPath() + File.separator + ".isolated-here-maps";
            ApplicationInfo ai = getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            String intentName = bundle.getString("com.tbs.generic.vansales");
            if(MapSettings.setIsolatedDiskCacheRootPath(diskCacheRoot, intentName)){
                initMapFragment();
            }
            tvReCenter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(btnStart.getText().toString().equalsIgnoreCase("Start")){
                        if(m_route!=null){
                            m_geoBoundingBox        = m_route.getBoundingBox();
                            map.zoomTo(m_geoBoundingBox, Map.Animation.BOW, Map.MOVE_PRESERVE_ORIENTATION);
                        }
                        else {
                            map = m_mapFragment.getMap();
                            map.setZoomLevel(map.getMaxZoomLevel() - 1);
                            map.setCenter(currentLocGeoCordinate, Map.Animation.NONE);// always  pass current latt lang
                        }
                    }
                    else {
                        map.setZoomLevel(map.getMaxZoomLevel() - 1);
                        map.setCenter(currentLocGeoCordinate, Map.Animation.NONE);
                    }

                }
            });

            ivCar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ivCar.setImageDrawable(getResources().getDrawable(R.drawable.car_icon));
                    ivTruck.setImageDrawable(getResources().getDrawable(R.drawable.trcuk_cion));
                    ivWalk.setImageDrawable(getResources().getDrawable(R.drawable.walk_icon));
                    createCarRoute();

                }
            });
            ivTruck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ivCar.setImageDrawable(getResources().getDrawable(R.drawable.car_icon));
                    ivTruck.setImageDrawable(getResources().getDrawable(R.drawable.trcuk_cion));
                    ivWalk.setImageDrawable(getResources().getDrawable(R.drawable.walk_icon));
                    createTruckRoute();
                }
            });

            btnStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (btnStart.getText().toString().equalsIgnoreCase("Start")) {
                        if (m_route != null) {
                            startNavigation();
                        }
                        else {
                            llNavigation.setVisibility(View.VISIBLE);
                            llInstructions.setVisibility(View.GONE);
                            llBottom.setVisibility(View.VISIBLE);
                            Toast.makeText(HereMapActivity.this, "please select Travel mode", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
//                        llNavigation.setVisibility(View.VISIBLE);
//                        llInstructions.setVisibility(View.GONE);
//                        llBottom.setVisibility(View.VISIBLE);
//                        btnStart.setText("Start");
//                        m_navigationManager.stop();
                    }
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Exception : "+e.getMessage());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_CODE_ASK_PERMISSIONS: {
                for (int index = 0; index < permissions.length; index++) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        if (!ActivityCompat
                                .shouldShowRequestPermissionRationale(this, permissions[index])) {
                            Toast.makeText(this, "Required permission " + permissions[index]
                                            + " not granted. "
                                            + "Please go to settings and turn on for sample app",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(this, "Required permission " + permissions[index]
                                    + " not granted", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                initializeMapsAndPositioning();
                break;
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void startNavigation() {
        showLoader("");
        tvReCenter.setVisibility(View.VISIBLE);
        llBottom.setVisibility(View.VISIBLE);
        llInstructions.setVisibility(View.VISIBLE);
        btnStart.setText("Started");
        map.setMapScheme(mapScheme);
        map.setTilt(60);
        map.setZoomLevel(map.getMaxZoomLevel()-1);
        map.setCenter(currentLocGeoCordinate, Map.Animation.BOW);

//        m_mapTransformCenter = new PointF(map.getTransformCenter().x, (map.getTransformCenter().y * 85 / 50));
//        map.setTransformCenter(m_mapTransformCente);
//        try {
//            Image icon = new Image();
//            m_positionIndicatorFixed = new MapMarker();
//            icon.setImageResource(R.drawable.car_marker);
//            m_positionIndicatorFixed.setIcon(icon);
//            m_positionIndicatorFixed.setVisible(true);
//            m_positionIndicatorFixed.setCoordinate(map.getCenter());
//            map.addMapObject(m_positionIndicatorFixed);
//            m_mapFragment.getPositionIndicator().setVisible(false);
//            m_navigationManager.setMap(map);
//            m_mapFragment.getPositionIndicator().setMarker(icon);
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }

        m_mapFragment.getPositionIndicator().setVisible(true);
        m_navigationManager.setSpeedWarningEnabled(true);
        m_navigationManager.setRoute(m_route);
        m_navigationManager.startNavigation(m_route);
        m_navigationManager.setMapUpdateMode(NavigationManager.MapUpdateMode.POSITION_ANIMATION);
        // NavigationManager.MapUpdateMode.ROAD_VIEW gives marker moves prope direction but alswys it keeps center
        if(Constants.Simulate){// just testing purpose, don't use in prod
            m_navigationManager.simulate(m_route, 15);// only testing purpose
        }
        m_navigationManager.addRealisticViewAspectRatio(NavigationManager.AspectRatio.AR_3x5);
        m_navigationManager.addRealisticViewListener(new WeakReference<NavigationManager.RealisticViewListener>(viewListener)); // no use of this
        m_mapFragment.addOnMapRenderListener(new OnMapRenderListener() {
//            @Override
//            public void onPreDraw() {
//                if (m_positionIndicatorFixed != null) {
//                    if (m_navigationManager.getMapUpdateMode().equals(NavigationManager.MapUpdateMode.ROADVIEW)) {
//                        if (!m_returningToRoadViewMode) {
//                            // when road view is active, we set the position indicator to align
//                            // with the current map transform center to synchronize map and map
//                            // marker movements.
//                            m_positionIndicatorFixed.setCoordinate(map.pixelToGeo(m_mapTransformCenter));
//                        }
//                    }
//                }
//            }

            @Override
            public void onPreDraw() {
                String renderer = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.FROYO) {
                    renderer = GLES20.glGetString(GLES20.GL_RENDERER);
                }
                if(renderer.contains("PowerVR SGX 544MP")) {
                    map.setExtrudedBuildingsVisible(true);
                }
            }
            @Override
            public void onPostDraw(boolean b, long l) {
            }
            @Override
            public void onSizeChanged(int i, int i1) {
            }
            @Override
            public void onGraphicsDetached() {
            }
            @Override
            public void onRenderBufferCreated() {
            }
        });
        m_navigationManager.addNavigationManagerEventListener(new WeakReference<NavigationManager.NavigationManagerEventListener>(new NavigationManager.NavigationManagerEventListener() {

            @Override
            public void onRunningStateChanged() {

//                Toast.makeText(getApplicationContext(), "Running state changed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNavigationModeChanged() {
//                Toast.makeText(getApplicationContext(), "Navigation mode changed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onEnded(NavigationManager.NavigationMode navigationMode) {
                Toast.makeText(getApplicationContext(), "Reached destination", Toast.LENGTH_SHORT).show();
                m_navigationManager.stop();
            }

            @Override
            public void onMapUpdateModeChanged(NavigationManager.MapUpdateMode mapUpdateMode) {
                m_navigationManager.setMapUpdateMode(mapUpdateMode);
//                Toast.makeText(getApplicationContext(), "Map update mode is changed to " + mapUpdateMode,
//                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRouteUpdated(Route route) {
                m_route = route;
//                coreRouter.calculateRoute(routePlan, routeListener);
//                Toast.makeText(getApplicationContext(), "Route updated", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCountryInfo(String s, String s1) {
//                Toast.makeText(getApplicationContext(), "Country info updated from " + s + " to " + s1,
//                        Toast.LENGTH_SHORT).show();
            }
        }));
        m_navigationManager.addGpsSignalListener(new WeakReference<NavigationManager.GpsSignalListener>(new NavigationManager.GpsSignalListener() {
            @Override
            public void onGpsLost() {
                Toast.makeText(HereMapActivity.this, "Lost GPS Signal", Toast.LENGTH_SHORT).show();
                super.onGpsLost();
            }

            @Override
            public void onGpsRestored() {
                super.onGpsRestored();
                Toast.makeText(HereMapActivity.this, "Restored GPS Signal", Toast.LENGTH_SHORT).show();
            }
        }));
        m_navigationManager.addRerouteListener(new WeakReference<NavigationManager.RerouteListener>(new NavigationManager.RerouteListener() {
            @Override
            public void onRerouteBegin() {
                Toast.makeText(getApplicationContext(), "reroute begin", Toast.LENGTH_SHORT).show();
            }
        }));
        m_navigationManager.addManeuverEventListener(new WeakReference<NavigationManager.ManeuverEventListener>(new NavigationManager.ManeuverEventListener() {
            @Override
            public void onManeuverEvent() {
                super.onManeuverEvent();
                Maneuver maneuver = m_navigationManager.getNextManeuver();
                if (maneuver != null) {
                    tvDirection.setText(""+maneuver.getTurn());
                    tvDirection.append(maneuver.getNextRoadName());
                    if(maneuver.getDistanceToNextManeuver() < 1000){
                        tvDirection.append("\n"+maneuver.getDistanceToNextManeuver()+ " mtr");
                    }
                    else {
                        tvDirection.append("\n"+(maneuver.getDistanceToNextManeuver()/1000)+ " km");
                    }
                }
            }
        }));
        m_navigationManager.addNewInstructionEventListener(new WeakReference<NavigationManager.NewInstructionEventListener>(new NavigationManager.NewInstructionEventListener() {
            @Override
            public void onNewInstructionEvent() {
                super.onNewInstructionEvent();

            }
        }));
        Maneuver maneuver = m_navigationManager.getNextManeuver();
        if(maneuver!=null){
            tvDistance.setText(""+maneuver.getDistanceToNextManeuver());
            int timeInSeconds = m_route.getTtaExcludingTraffic(Route.WHOLE_ROUTE).getDuration()/60;
            tvTime.setText(""+timeInSeconds + " min");
        }
        NavigationManager.RoadView roadView = m_navigationManager.getRoadView();
        roadView.addListener(new WeakReference<NavigationManager.RoadView.Listener>(mNavigationManagerRoadViewListener));
        roadView.setOrientation(NavigationManager.RoadView.Orientation.DYNAMIC);

        TrafficUpdater trafficUpdater = TrafficUpdater.getInstance();
        trafficUpdater.enableUpdate(true);
//        CameraUpdate cameraUpdate = m_mapFragment.getMap().viroadView.getCamera().calculateEnclosingCameraUpdate(
//                route.getBoundingBox(),
//                new Padding(10, 10, 10, 10));
//        mapView.getCamera().updateCamera(cameraUpdate);

    }

    private void calculateTtaUsingDownloadedTraffic() {
        TrafficUpdater.getInstance().enableUpdate(true);
        m_requestInfo = TrafficUpdater.getInstance().request(
                m_route, new TrafficUpdater.Listener() {
                    @Override
                    public void onStatusChanged(TrafficUpdater.RequestState requestState) {
                        final RouteTta ttaDownloaded = m_route.getTtaUsingDownloadedTraffic(Route.WHOLE_ROUTE);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                tvDTime.setText("Rem : " + ttaDownloaded.getDuration()/60 + " min");
                            }
                        });
                    }
                });
    }

    private void initMapFragment(){
        if (m_mapFragment != null) {
            showLoader("");
            m_mapFragment.init(new OnEngineInitListener() {
                @Override
                public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
                    hideLoader();
                    if (error == Error.NONE) {
                        map = m_mapFragment.getMap();
                        map.setZoomLevel(map.getMaxZoomLevel()-1);
                        map.setCenter(currentLocGeoCordinate = new GeoCoordinate(Constants.Src_Lattitude, Constants.Src_Longitude), Map.Animation.NONE);// always  pass current latt lang
                        map.addTransformListener(onTransformListener);
                        map.setTilt(60);
                        m_navigationManager = NavigationManager.getInstance();
                        mPositioningManager = PositioningManager.getInstance();
                        mHereLocation       = LocationDataSourceHERE.getInstance(new StatusListener() {
                            @Override
                            public void onOfflineModeChanged(boolean offline) {
                                // called when offline mode changes
                            }

                            @Override
                            public void onAirplaneModeEnabled() {
                                // called when airplane mode is enabled
                            }

                            @Override
                            public void onWifiScansDisabled() {
                                // called when Wi-Fi scans are disabled
                            }

                            @Override
                            public void onBluetoothDisabled() {
                                // called when Bluetooth is disabled
                            }

                            @Override
                            public void onCellDisabled() {
                                // called when Cell radios are switch off
                            }

                            @Override
                            public void onGnssLocationDisabled() {
                                Toast.makeText(HereMapActivity.this, "Please enable GPS", Toast.LENGTH_SHORT).show();
                                // called when GPS positioning is disabled
                            }

                            @Override
                            public void onNetworkLocationDisabled() {
                                // called when network positioning is disabled
                            }

                            @Override
                            public void onServiceError(ServiceError serviceError) {
                                // called on HERE service error
                            }

                            @Override
                            public void onPositioningError(PositioningError positioningError) {
                                // called when positioning fails
                            }

                            @Override
                            @SuppressWarnings("deprecation")
                            public void onWifiIndoorPositioningNotAvailable() {
                                // called when running on Android 9.0 (Pie) or newer
                            }

                            @Override
                            public void onWifiIndoorPositioningDegraded() {
                                // called when running on Android 9.0 (Pie) or newer
                            }
                        });//leave it
                        if (mHereLocation == null) {
                            Toast.makeText(HereMapActivity.this, "LocationDataSourceHERE.getInstance(): failed, exiting", Toast.LENGTH_LONG).show();
                            finish();
                        }// finish  leave it
                        m_navigationManager.setMapUpdateMode(NavigationManager.MapUpdateMode.ROADVIEW);
                        m_navigationManager.setRealisticViewMode(NavigationManager.RealisticViewMode.DAY);
                        m_navigationManager.setMap(map);
//                        mPositioningManager.setMapMatchingEnabled(true);
//                        mPositioningManager.enableProbeDataCollection(true);
                        mPositioningManager.setDataSource(mHereLocation);
                        mPositioningManager.addListener(new WeakReference<PositioningManager.OnPositionChangedListener>(onPositionChangedListener));
                        mPositioningManager.start(PositioningManager.LocationMethod.GPS_NETWORK);
//                        if (mPositioningManager.start(PositioningManager.LocationMethod.GPS_NETWORK);) {
                            try {
                                Image image = new Image();
                                image.setImageResource(R.drawable.image_three);
                                m_mapFragment.getPositionIndicator().setVisible(true);
                                m_mapFragment.getPositionIndicator().setMarker(image);
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
//                        }
//                        else {
//                            Toast.makeText(HereMapActivity.this, "PositioningManager.start: failed, exiting", Toast.LENGTH_LONG).show();
//                            finish();
//                        }
                    }
                    else {
                        new AlertDialog.Builder(HereMapActivity.this).setMessage(
                                "Error : " + error.name() + "\n\n" + error.getDetails())
                                .setTitle(R.string.engine_init_error)
                                .setNegativeButton(android.R.string.cancel,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                                finish();
                                            }
                                        }).create().show();
                    }  // leave it if not not initialised when ERROR
                }
            });
        }
    }

    private CoreRouter coreRouter;
    private RoutePlan routePlan;
    private Router.Listener routeListener;

    private void createTruckRoute() {
        try {
            showLoader("");
            tvDirection.setText("");
            tvTime.setText("");
            tvDTime.setText("");
            tvDDistance.setText("");
            btnStart.setText("Start");
            ivCar.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            ivTruck.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_bright));
            if (map != null && mapRoute != null) {
                map.removeMapObject(mapRoute);
                mapRoute = null;
            }
//            MapLoader.getInstance().selectDataGroup(MapPackage.SelectableDataGroup.ScooterAttributes);

            coreRouter       = new CoreRouter();
            routePlan         = new RoutePlan();
            mapScheme         = Map.Scheme.TRUCKNAV_DAY;
            RouteOptions routeOptions   = new RouteOptions();
            routeOptions.setTransportMode(RouteOptions.TransportMode.TRUCK);
            routeOptions.setRouteType(RouteOptions.Type.FASTEST);//shortest and Balance wont support for Truck
            routeOptions.setTruckTunnelCategory(RouteOptions.TunnelCategory.E)
//                    .setTruckLength(135.25f)
//                    .setTruckHeight(15.6f)
                    .setTruckLength(25.25f)
                    .setTruckHeight(2.6f)
                    .setTruckTrailersCount(1);
            routePlan.setRouteOptions(routeOptions);
//            Image image = new Image();
//            image.setImageResource(R.drawable.marker2);
//            MapMarker customMarker = new MapMarker(new GeoCoordinate(Constants.Src_Lattitude, Constants.Src_Longitude, 0.0), image);
//            map.addMapObject(customMarker);
            Image destImage = new Image();
            destImage.setImageResource(R.drawable.image_one);
            MapMarker customMarker2 = new MapMarker(new GeoCoordinate(Constants.Des_Lattitude, Constants.Des_Longitude, 0.0), destImage);
            map.addMapObject(customMarker2);

            Image truckImage = new Image();
            truckImage.setImageResource(R.drawable.car_marker);// change icon
            map.getPositionIndicator().setVisible(true);
            map.getPositionIndicator().setMarker(truckImage);
//            map.getPositionIndicator().setSmoothPositionChange(true);
            map.getPositionIndicator().setSmoothPositionChange(true);
//            map.getPositionIndicator().setAccuracyIndicatorVisible(true);
//            map.getPositionIndicator().setAccuracyIndicatorColor(getResources().getColor(android.R.color.holo_blue_dark));
            final RouteWaypoint startPoint = new RouteWaypoint(currentLocGeoCordinate = new GeoCoordinate(Constants.Src_Lattitude, Constants.Src_Longitude));
            RouteWaypoint destination = new RouteWaypoint(destLocGeoCordinate = new GeoCoordinate(Constants.Des_Lattitude, Constants.Des_Longitude));
            startPoint.setSelectiveMatchingRadius(5);
            routePlan.addWaypoint(startPoint);
            routePlan.addWaypoint(destination);
            coreRouter.calculateRoute(routePlan,
                    routeListener = new Router.Listener<List<RouteResult>, RoutingError>() {
                        @Override
                        public void onProgress(int progress) {
                            Log.e(TAG, "onProgress() => progress : "+progress);
                        }

                        @Override
                        public void onCalculateRouteFinished(List<RouteResult> routeResults, RoutingError routingError) {
                            hideLoader();
                            if (routingError == RoutingError.NONE) {
                                if (mapRoute != null) {
                                    map.removeMapObject(mapRoute);
                                }
                                if (routeResults.get(0).getRoute() != null) {
                                    m_route                 = routeResults.get(0).getRoute();
                                    mapRoute                = new MapRoute();
                                    mapRoute.setManeuverNumberVisible(true);
                                    mapRoute.setColor(getResources().getColor(android.R.color.holo_green_dark));
                                    mapRoute.setOutlineColor(getResources().getColor(android.R.color.holo_purple));
                                    mapRoute.setManeuverNumberColor(getResources().getColor(android.R.color.holo_orange_dark));
                                    mapRoute.setTraveledColor(getResources().getColor(android.R.color.holo_red_dark));
                                    mapRoute.setTrafficEnabled(true);
                                    mapRoute.setRoute(m_route);
                                    mapRoute.setRenderType(MapRoute.RenderType.USER_DEFINED);
                                    m_geoBoundingBox        = m_route.getBoundingBox();
//                                    m_geoBoundingBox.setCoordinates(currentLocGeoCordinate, destLocGeoCordinate);
//                                    m_geoBoundingBox.resizeToCenter(currentLocGeoCordinate);
                                    map.addMapObject(mapRoute);
                                    map.zoomTo(m_geoBoundingBox, Map.Animation.BOW, Map.MOVE_PRESERVE_ORIENTATION);

                                    RouteTta routeTta = m_route.getTtaUsingDownloadedTraffic(Route.WHOLE_ROUTE);
                                    tvDDistance.setText(" "+decimalFormat.format(currentLocGeoCordinate.distanceTo(m_route.getDestination())/1000)+" km");
                                    int timeInSeconds = m_route.getTtaIncludingTraffic(Route.WHOLE_ROUTE).getDuration()/60;
                                    tvDTime.setText(""+(routeTta.getDuration()/60) + " min");
                                    calculateTtaUsingDownloadedTraffic();
                                }
                                else {
                                    Log.e(TAG, "onProgress() =>  : routeResults.get(0).getRoute()");
                                    Toast.makeText(getApplicationContext(), "Error : routeResults.get(0).getRoute()", Toast.LENGTH_LONG).show();
                                }
                            }
                            else if(routingError==RoutingError.NO_START_POINT) {
                                Log.e(TAG, "onProgress() =>RoutingError.NO_START_POINT  : Source Location not found");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.NO_START_POINT Source Location not found", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError == RoutingError.GRAPH_DISCONNECTED){
                                Log.e(TAG, "onProgress() =>RoutingError.GRAPH_DISCONNECTED  : No route was found");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.GRAPH_DISCONNECTED : No route was found", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.NO_END_POINT){
                                Log.e(TAG, "onProgress() =>RoutingError.NO_END_POINT  : Destination Location not found");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.NO_END_POINT : Destination Location not found", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.CANNOT_DO_PEDESTRIAN){
                                Log.e(TAG, "onProgress() =>RoutingError.CANNOT_DO_PEDESTRIAN  : possibly the route is too long");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.CANNOT_DO_PEDESTRIAN : possibly the route is too long", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.INSUFFICIENT_MAP_DATA){
                                Log.e(TAG, "onProgress() =>RoutingError.INSUFFICIENT_MAP_DATA  : The route cannot be calculated because there is not enough local map data to perform route calculation.");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.INSUFFICIENT_MAP_DATA : The route cannot be calculated because there is not enough local map data to perform route calculation.", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.INVALID_CREDENTIALS){
                                Log.e(TAG, "onProgress() =>RoutingError.INVALID_CREDENTIALS  : The route cannot be calculated because the HERE Developer credentials are invalid or were not provided.");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.INVALID_CREDENTIALS : The route cannot be calculated because the HERE Developer credentials are invalid or were not provided. ", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.INVALID_OPERATION){
                                Log.e(TAG, "onProgress() => RoutingError.INVALID_OPERATION  : The operation is not allowed at this time because another request is in progress.");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.INVALID_OPERATION : The operation is not allowed at this time because another request is in progress.", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.INVALID_PARAMETERS){
                                Log.e(TAG, "onProgress() => RoutingError.INVALID_PARAMETERS  : Parameters passed were invalid");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.INVALID_PARAMETERS : Parameters passed were invalid.", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.NETWORK_COMMUNICATION){
                                Log.e(TAG, "onProgress() => RoutingError.NETWORK_COMMUNICATION  : The online route calculation request failed because of a networking error");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.NETWORK_COMMUNICATION : The online route calculation request failed because of a networking error.", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.NO_CONNECTIVITY){
                                Log.e(TAG, "onProgress() => RoutingError.NO_CONNECTIVITY : No internet connection is available.");
                                Toast.makeText(getApplicationContext(), "Error: RoutingError.NO_CONNECTIVITY : No internet connection is available.", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.OPERATION_NOT_ALLOWED){
                                Log.e(TAG, "onProgress() => RoutingError.OPERATION_NOT_ALLOWED : Access to this operation is denied.");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.OPERATION_NOT_ALLOWED : Access to this operation is denied.", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.OUT_OF_MEMORY){
                                Log.e(TAG, "onProgress() => RoutingError.OUT_OF_MEMORY : An out-of-memory error prevented calculation.");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.OUT_OF_MEMORY : An out-of-memory error prevented calculation.", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.ROUTE_CORRUPTED){
                                Log.e(TAG, "onProgress() => RoutingError.ROUTE_CORRUPTED : Could not decode the route as received from the server.");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.ROUTE_CORRUPTED : Could not decode the route as received from the server.", Toast.LENGTH_LONG).show();
                            }
                            else {
                                Log.e(TAG, "onProgress() => route calculation returned error code.");
                                Toast.makeText(getApplicationContext(), "Error : route calculation returned error code: " + routingError.toString(), Toast.LENGTH_LONG).show();
                            }

                        }
                    });

        }
        catch (Exception e) {
            Log.e("HERE", e.getMessage());
        }
    }

    private void createCarRoute() {
        try {
            showLoader("");
            tvDirection.setText("");
            tvTime.setText("");
            tvDTime.setText("");
            tvDDistance.setText("");
            btnStart.setText("Start");
            ivTruck.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            ivCar.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_bright));
            if (map != null && mapRoute != null) {
                map.removeMapObject(mapRoute);
                mapRoute = null;
            }
//            MapLoader.getInstance().selectDataGroup(MapPackage.SelectableDataGroup.ScooterAttributes);

            coreRouter       = new CoreRouter();
            routePlan         = new RoutePlan();
            mapScheme         = Map.Scheme.CARNAV_DAY_GREY;
            RouteOptions routeOptions   = new RouteOptions();
            routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
            routeOptions.setRouteType(RouteOptions.Type.FASTEST);//shortest and Balance wont support for Truck
//            routeOptions.setTruckTunnelCategory(RouteOptions.TunnelCategory.E)
//                    .setTruckLength(135.25f)
//                    .setTruckHeight(15.6f)
//                    .setTruckLength(25.25f)
//                    .setTruckHeight(2.6f)
//                    .setTruckTrailersCount(1);
            routePlan.setRouteOptions(routeOptions);
//            Image image = new Image();
//            image.setImageResource(R.drawable.marker2);
//            MapMarker customMarker = new MapMarker(new GeoCoordinate(Constants.Src_Lattitude, Constants.Src_Longitude, 0.0), image);
//            map.addMapObject(customMarker);
            Image destImage = new Image();
            destImage.setImageResource(R.drawable.image_one);
            MapMarker customMarker2 = new MapMarker(new GeoCoordinate(Constants.Des_Lattitude, Constants.Des_Longitude, 0.0), destImage);
            map.addMapObject(customMarker2);

            Image truckImage = new Image();
            truckImage.setImageResource(R.drawable.car_marker);// change icon
            map.getPositionIndicator().setVisible(true);
            map.getPositionIndicator().setMarker(truckImage);
//            map.getPositionIndicator().setSmoothPositionChange(true);
            map.getPositionIndicator().setSmoothPositionChange(true);
//            map.getPositionIndicator().setAccuracyIndicatorVisible(true);
//            map.getPositionIndicator().setAccuracyIndicatorColor(getResources().getColor(android.R.color.holo_blue_dark));
            final RouteWaypoint startPoint = new RouteWaypoint(currentLocGeoCordinate = new GeoCoordinate(Constants.Src_Lattitude, Constants.Src_Longitude));
            RouteWaypoint destination = new RouteWaypoint(destLocGeoCordinate = new GeoCoordinate(Constants.Des_Lattitude, Constants.Des_Longitude));
            startPoint.setSelectiveMatchingRadius(5);
            routePlan.addWaypoint(startPoint);
            routePlan.addWaypoint(destination);
            coreRouter.calculateRoute(routePlan,
                    routeListener = new Router.Listener<List<RouteResult>, RoutingError>() {
                        @Override
                        public void onProgress(int progress) {
                            Log.e(TAG, "onProgress() => progress : "+progress);
                        }

                        @Override
                        public void onCalculateRouteFinished(List<RouteResult> routeResults, RoutingError routingError) {
                            hideLoader();
                            if (routingError == RoutingError.NONE) {
                                if (mapRoute != null) {
                                    map.removeMapObject(mapRoute);
                                }
                                if (routeResults.get(0).getRoute() != null) {
                                    m_route                 = routeResults.get(0).getRoute();
                                    mapRoute                = new MapRoute();
                                    mapRoute.setManeuverNumberVisible(true);
                                    mapRoute.setColor(getResources().getColor(android.R.color.holo_green_dark));
                                    mapRoute.setOutlineColor(getResources().getColor(android.R.color.holo_purple));
                                    mapRoute.setManeuverNumberColor(getResources().getColor(android.R.color.holo_orange_dark));
                                    mapRoute.setTraveledColor(getResources().getColor(android.R.color.holo_red_dark));
                                    mapRoute.setTrafficEnabled(true);
                                    mapRoute.setRoute(m_route);
                                    mapRoute.setRenderType(MapRoute.RenderType.USER_DEFINED);
                                    m_geoBoundingBox        = m_route.getBoundingBox();
//                                    m_geoBoundingBox.setCoordinates(currentLocGeoCordinate, destLocGeoCordinate);
//                                    m_geoBoundingBox.resizeToCenter(currentLocGeoCordinate);
                                    map.addMapObject(mapRoute);
                                    map.zoomTo(m_geoBoundingBox, Map.Animation.BOW, Map.MOVE_PRESERVE_ORIENTATION);

                                    RouteTta routeTta = m_route.getTtaUsingDownloadedTraffic(Route.WHOLE_ROUTE);
                                    tvDDistance.setText(" "+decimalFormat.format(currentLocGeoCordinate.distanceTo(m_route.getDestination())/1000)+" km");
                                    int timeInSeconds = m_route.getTtaIncludingTraffic(Route.WHOLE_ROUTE).getDuration()/60;
                                    tvDTime.setText(""+(routeTta.getDuration()/60) + " min");
                                    calculateTtaUsingDownloadedTraffic();
                                }
                                else {
                                    Log.e(TAG, "onProgress() =>  : routeResults.get(0).getRoute()");
                                    Toast.makeText(getApplicationContext(), "Error : routeResults.get(0).getRoute()", Toast.LENGTH_LONG).show();
                                }
                            }
                            else if(routingError==RoutingError.NO_START_POINT) {
                                Log.e(TAG, "onProgress() =>RoutingError.NO_START_POINT  : Source Location not found");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.NO_START_POINT Source Location not found", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError == RoutingError.GRAPH_DISCONNECTED){
                                Log.e(TAG, "onProgress() =>RoutingError.GRAPH_DISCONNECTED  : No route was found");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.GRAPH_DISCONNECTED : No route was found", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.NO_END_POINT){
                                Log.e(TAG, "onProgress() =>RoutingError.NO_END_POINT  : Destination Location not found");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.NO_END_POINT : Destination Location not found", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.CANNOT_DO_PEDESTRIAN){
                                Log.e(TAG, "onProgress() =>RoutingError.CANNOT_DO_PEDESTRIAN  : possibly the route is too long");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.CANNOT_DO_PEDESTRIAN : possibly the route is too long", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.INSUFFICIENT_MAP_DATA){
                                Log.e(TAG, "onProgress() =>RoutingError.INSUFFICIENT_MAP_DATA  : The route cannot be calculated because there is not enough local map data to perform route calculation.");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.INSUFFICIENT_MAP_DATA : The route cannot be calculated because there is not enough local map data to perform route calculation.", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.INVALID_CREDENTIALS){
                                Log.e(TAG, "onProgress() =>RoutingError.INVALID_CREDENTIALS  : The route cannot be calculated because the HERE Developer credentials are invalid or were not provided.");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.INVALID_CREDENTIALS : The route cannot be calculated because the HERE Developer credentials are invalid or were not provided. ", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.INVALID_OPERATION){
                                Log.e(TAG, "onProgress() => RoutingError.INVALID_OPERATION  : The operation is not allowed at this time because another request is in progress.");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.INVALID_OPERATION : The operation is not allowed at this time because another request is in progress.", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.INVALID_PARAMETERS){
                                Log.e(TAG, "onProgress() => RoutingError.INVALID_PARAMETERS  : Parameters passed were invalid");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.INVALID_PARAMETERS : Parameters passed were invalid.", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.NETWORK_COMMUNICATION){
                                Log.e(TAG, "onProgress() => RoutingError.NETWORK_COMMUNICATION  : The online route calculation request failed because of a networking error");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.NETWORK_COMMUNICATION : The online route calculation request failed because of a networking error.", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.NO_CONNECTIVITY){
                                Log.e(TAG, "onProgress() => RoutingError.NO_CONNECTIVITY : No internet connection is available.");
                                Toast.makeText(getApplicationContext(), "Error: RoutingError.NO_CONNECTIVITY : No internet connection is available.", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.OPERATION_NOT_ALLOWED){
                                Log.e(TAG, "onProgress() => RoutingError.OPERATION_NOT_ALLOWED : Access to this operation is denied.");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.OPERATION_NOT_ALLOWED : Access to this operation is denied.", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.OUT_OF_MEMORY){
                                Log.e(TAG, "onProgress() => RoutingError.OUT_OF_MEMORY : An out-of-memory error prevented calculation.");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.OUT_OF_MEMORY : An out-of-memory error prevented calculation.", Toast.LENGTH_LONG).show();
                            }
                            else if(routingError==RoutingError.ROUTE_CORRUPTED){
                                Log.e(TAG, "onProgress() => RoutingError.ROUTE_CORRUPTED : Could not decode the route as received from the server.");
                                Toast.makeText(getApplicationContext(), "Error : RoutingError.ROUTE_CORRUPTED : Could not decode the route as received from the server.", Toast.LENGTH_LONG).show();
                            }
                            else {
                                Log.e(TAG, "onProgress() => route calculation returned error code.");
                                Toast.makeText(getApplicationContext(), "Error : route calculation returned error code: " + routingError.toString(), Toast.LENGTH_LONG).show();
                            }

                        }
                    });

        }
        catch (Exception e) {
            Log.e("HERE", e.getMessage());
        }
    }


    private void remainingDistance(GeoCoordinate currentLoc){
        if(destLocGeoCordinate !=null){
            double remainingDistance = currentLoc.distanceTo(destLocGeoCordinate);
            if(remainingDistance < 1000){
                tvDDistance.setText("Rem : " +decimalFormat.format(remainingDistance)+" mtr");
            }
            else {
                tvDDistance.setText("Rem : " +decimalFormat.format(remainingDistance/1000)+" km");
            }
            Maneuver maneuver = m_route.getFirstManeuver();
            if(maneuver!=null){
                tvDirection.setText(maneuver.getTurn().name());
                tvDirection.setText(maneuver.getNextRoadName());
                if(remainingDistance < 1000){
                    tvDirection.append("\n"+maneuver.getDistanceToNextManeuver()+ " mtr");
                }
                else {
                    tvDirection.append("\n"+(maneuver.getDistanceToNextManeuver()/1000)+ " km");
                }
            }
            RouteTta routeTta = m_route.getTtaUsingDownloadedTraffic(Route.WHOLE_ROUTE);
            tvDTime.setText(""+(routeTta.getDuration()/60) + " min");
        }
    }
    private void updateLocationInfo(PositioningManager.LocationMethod locationMethod, GeoPosition geoPosition) {

        final StringBuilder sb = new StringBuilder();
        final GeoCoordinate coord = geoPosition.getCoordinate();
//        sb.append("Type: ").append(String.format(Locale.US, "%s\n", locationMethod.name()));
//        sb.append("Coordinate:").append(String.format(Locale.US, "%.6f, %.6f\n", coord.getLatitude(), coord.getLongitude()));
        if (coord.getAltitude() != GeoCoordinate.UNKNOWN_ALTITUDE) {
//            sb.append("Altitude:").append(String.format(Locale.US, "%.2fm\n", coord.getAltitude()));
        }
        if (geoPosition.getHeading() != GeoPosition.UNKNOWN) {
            sb.append("Heading:").append(String.format(Locale.US, "%.2f\n", geoPosition.getHeading()));
        }
        if (geoPosition.getSpeed() != GeoPosition.UNKNOWN) {
            sb.append("Speed:").append(String.format(Locale.US, "%.2f m/s\n", geoPosition.getSpeed()));
        }
        if (geoPosition.getSpeed() != GeoPosition.UNKNOWN) {
            btnStart.setText("Speed:");
            btnStart.append(String.format(Locale.US, "%.2f m/s\n", geoPosition.getSpeed()));
        }
//        if (geoPosition.getBuildingName() != null) {
//            sb.append("Building: ").append(geoPosition.getBuildingName());
//            if (geoPosition.getBuildingId() != null) {
//                sb.append(" (").append(geoPosition.getBuildingId()).append(")\n");
//            } else {
//                sb.append("\n");
//            }
//        }
//        if (geoPosition.getFloorId() != null) {
//            sb.append("Floor: ").append(geoPosition.getFloorId()).append("\n");
//        }
//        sb.deleteCharAt(sb.length() - 1);
//        tvDTime.setText(sb.toString());
        Maneuver maneuver = m_navigationManager.getNextManeuver();
//        if(m_navigationManager.getDestinationDistance() < 1000) {// not wokring returning -1
//            tvDDistance.setText("" + m_navigationManager.getDestinationDistance() + " mts");
//        }
//        else {
//            tvDDistance.setText("" + decimalFormat.format(m_navigationManager.getDestinationDistance() /1000)+ " km");
//        }
        if(maneuver!=null){
            tvDirection.setText(""+maneuver.getTurn().name());
//            tvDirection.setText(""+maneuver.getTurn().name());
            if(maneuver.getDistanceToNextManeuver() < 1000){
                tvDistance.setText("Next : " +maneuver.getDistanceToNextManeuver()+" mtr");
            }
            else {
                tvDistance.setText("Next : " +(decimalFormat.format(maneuver.getDistanceToNextManeuver()/1000))+" km");
            }
            if((m_route.getTtaIncludingTraffic(Route.WHOLE_ROUTE))!=null){
                int timeInSeconds = m_route.getTtaUsingDownloadedTraffic(Route.WHOLE_ROUTE).getDuration()/60;
                tvTime.setText(""+timeInSeconds + " min");
            }
        }


    }

    private Map.OnTransformListener onTransformListener = new Map.OnTransformListener(){

        @Override
        public void onMapTransformStart() {
            mTransforming = true;
        }

        @Override
        public void onMapTransformEnd(MapState mapState) {
            mTransforming = false;
            if (mPendingUpdate != null) {
                mPendingUpdate.run();
                mPendingUpdate = null;
            }
        }
    };

    private PositioningManager.OnPositionChangedListener onPositionChangedListener = new PositioningManager.OnPositionChangedListener() {
        @Override
        public void onPositionUpdated(final PositioningManager.LocationMethod locationMethod, final GeoPosition geoPosition, final boolean mapMatched) {
            hideLoader();
            currentLocGeoCordinate = geoPosition.getCoordinate();
            if (mTransforming) {
                mPendingUpdate = new Runnable() {
                    @Override
                    public void run() {
                        onPositionUpdated(locationMethod, geoPosition, mapMatched);
                    }
                };
            }
            else {
                if(mapRoute == null){
                    map.setCenter(currentLocGeoCordinate, Map.Animation.BOW);
                }
                Constants.Src_Lattitude = geoPosition.getCoordinate().getLatitude();
                Constants.Src_Longitude = geoPosition.getCoordinate().getLongitude();
                updateLocationInfo(locationMethod, geoPosition);
            }
//            if (NavigationManager.getInstance().getMapUpdateMode().equals(NavigationManager.MapUpdateMode.NONE) && !m_returningToRoadViewMode)
//                // use this updated position when map is not updated by RoadView.
//                m_positionIndicatorFixed.setCoordinate(geoPosition.getCoordinate());


        }

        @Override
        public void onPositionFixChanged(PositioningManager.LocationMethod locationMethod, PositioningManager.LocationStatus locationStatus) {

        }
    };

    private PointF m_mapTransformCenter;
    private boolean m_returningToRoadViewMode = false;
    private MapMarker m_positionIndicatorFixed;
    private NavigationManager.RoadView.Listener mNavigationManagerRoadViewListener = new NavigationManager.RoadView.Listener() {
        @Override
        public void onPositionChanged(GeoCoordinate geoCoordinate) {
            remainingDistance(geoCoordinate);
//            m_mapTransformCenter = map.projectToPixel (geoCoordinate).getResult();

        }
    };


    private NavigationManager.RealisticViewListener viewListener = new NavigationManager.RealisticViewListener(){

        @Override
        public void onRealisticViewNextManeuver(NavigationManager.AspectRatio aspectRatio, Image image, Image image1) {
            super.onRealisticViewNextManeuver(aspectRatio, image, image1);

        }

        @Override
        public void onRealisticViewShow(NavigationManager.AspectRatio aspectRatio, Image image, Image image1) {
            super.onRealisticViewShow(aspectRatio, image, image1);
        }
    };

    @Override
    public void initialize() {

    }

    @Override
    public void initializeControls() {
        btnStart            = findViewById(R.id.btnStart);
        ivCar               = findViewById(R.id.ivCar);
        ivTruck             = findViewById(R.id.ivTruck);
        ivWalk              = findViewById(R.id.ivWalk);
        tvReCenter          = findViewById(R.id.tvReCenter);
        llInstructions      = findViewById(R.id.llInstructions);
        tvDTime             = findViewById(R.id.tvDTime);
        tvDDistance         = findViewById(R.id.tvDDistance);
        tvTime              = findViewById(R.id.tvTime);
        tvDistance          = findViewById(R.id.tvDistance);
        llNavigation        = findViewById(R.id.ll1);
        tvDirection         = findViewById(R.id.tvDirection);
        llBottom            = findViewById(R.id.llBottom);
    }
}
