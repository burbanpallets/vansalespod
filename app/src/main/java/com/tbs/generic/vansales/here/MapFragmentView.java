/*
 * Copyright (c) 2011-2020 HERE Europe B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tbs.generic.vansales.here;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.guidance.NavigationManager;
import com.here.android.mpa.guidance.TrafficUpdater;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RouteTta;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.routing.Router;
import com.here.android.mpa.routing.RoutingError;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.R;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.List;

/**
 * This class encapsulates the properties and functionality of the Map view.It also triggers a
 * turn-by-turn navigation from HERE Burnaby office to Langley BC.There is a sample voice skin
 * bundled within the SDK package to be used out-of-box, please refer to the Developer's guide for
 * the usage.
 */
public class MapFragmentView {

    private DecimalFormat decimalFormat = new DecimalFormat("0.00");
    private AndroidXMapFragment m_mapFragment;
    private BaseActivity m_activity;
    private Button m_naviControlButton;
    private ImageView ivCar, ivTruck, ivRecenter;
    private TextView tvEstTime, tvEstDistance, tvNextTurn;
    private Map m_map;
    private NavigationManager m_navigationManager;
    private PositioningManager positioningManager;
    private GeoCoordinate currentLocGeoCordinate, destLocGeoCordinate;
    private GeoBoundingBox m_geoBoundingBox;
    private Route m_route;
    private MapRoute mapRoute;
    private int vehicleType = 0;
    private boolean m_foregroundServiceStarted;

    public MapFragmentView(TurnByTurnNavigationActivity activity) {
        m_activity = activity;
        ivCar               = m_activity.findViewById(R.id.ivCar);
        ivTruck             = m_activity.findViewById(R.id.ivTruck);
        ivRecenter          = m_activity.findViewById(R.id.ivRecenter);
        tvEstTime           = m_activity.findViewById(R.id.tvEstTime);
        tvEstDistance       = m_activity.findViewById(R.id.tvEstDistance);
        tvNextTurn          = m_activity.findViewById(R.id.tvNextTurn);

        initMapFragment();
        initNaviControlButton();
        ivTruck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivCar.setImageDrawable(m_activity.getResources().getDrawable(R.drawable.car1));
                ivTruck.setImageDrawable(m_activity.getResources().getDrawable(R.drawable.truck2));
//                ivTruck.setBackgroundColor(m_activity.getResources().getColor(R.color.colorAccent));
//                ivCar.setBackgroundColor(m_activity.getResources().getColor(android.R.color.transparent));
                vehicleType = 2;
                if (m_map != null && mapRoute != null) {
                    m_map.removeMapObject(mapRoute);
                    mapRoute = null;
                }
                if(m_navigationManager!=null){
                    m_navigationManager.stop();
                    m_navigationManager.removeNavigationManagerEventListener(m_navigationManagerEventListener);
                    m_navigationManager.removePositionListener(m_positionListener);
                    m_naviControlButton.setText(R.string.start_navi);
                }
                m_route = null;
                createRoute();
            }
        });
        ivCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivCar.setImageDrawable(m_activity.getResources().getDrawable(R.drawable.car2));
                ivTruck.setImageDrawable(m_activity.getResources().getDrawable(R.drawable.truck1));
                vehicleType = 1;
                if (m_map != null && mapRoute != null) {
                    m_map.removeMapObject(mapRoute);
                    mapRoute = null;
                }
                if(m_navigationManager!=null){
                    m_navigationManager.stop();
                    m_navigationManager.removeNavigationManagerEventListener(m_navigationManagerEventListener);
                    m_navigationManager.removePositionListener(m_positionListener);
                    m_naviControlButton.setText(R.string.start_navi);
                }
                m_route = null;
                createRoute();
            }
        });
        ivRecenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(m_route!=null){
                    m_geoBoundingBox        = m_route.getBoundingBox();
                    m_map.zoomTo(m_geoBoundingBox, Map.Animation.BOW, Map.MOVE_PRESERVE_ORIENTATION);
                }
                else {
                    m_map.setZoomLevel((m_map.getMaxZoomLevel()-1));
                    m_map.setCenter(currentLocGeoCordinate = new GeoCoordinate(Constants.Src_Lattitude, Constants.Src_Longitude), Map.Animation.NONE);
//                    m_mapFragment.getMap().setCenter(currentLocGeoCordinate, Map.Animation.NONE);// always  pass current latt lang
                }
            }
        });
    }

    private AndroidXMapFragment getMapFragment() {
        return (AndroidXMapFragment) m_activity.getSupportFragmentManager().findFragmentById(R.id.mapfragment);
    }

    private void initMapFragment() {
        /* Locate the mapFragment UI element */
        m_mapFragment = getMapFragment();

        // Set path of disk cache
        String diskCacheRoot = m_activity.getFilesDir().getPath()
                + File.separator + ".isolated-here-maps";
        // Retrieve intent name from manifest
        String intentName = "";
        try {
            ApplicationInfo ai = m_activity.getPackageManager().getApplicationInfo(m_activity.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            intentName = bundle.getString("com.tbs.generic.vansales");
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(this.getClass().toString(), "Failed to find intent name, NameNotFound: " + e.getMessage());
        }

        boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(diskCacheRoot, intentName);
        if (!success) {
            // Setting the isolated disk cache was not successful, please check if the path is valid and
            // ensure that it does not match the default location
            // (getExternalStorageDirectory()/.here-maps).
            // Also, ensure the provided intent name does not match the default intent name.
        } else {
            if (m_mapFragment != null) {
                /* Initialize the AndroidXMapFragment, results will be given via the called back. */
                m_mapFragment.init(new OnEngineInitListener() {
                    @Override
                    public void onEngineInitializationCompleted(Error error) {

                        if (error == Error.NONE) {
                            m_map = m_mapFragment.getMap();
                            m_map.setCenter(currentLocGeoCordinate = new GeoCoordinate(Constants.Src_Lattitude, Constants.Src_Longitude), Map.Animation.NONE);
                            //Put this call in Map.onTransformListener if the animation(Linear/Bow)
                            //is used in setCenter()
                            m_map.setTilt(25);
                            m_map.setZoomLevel((m_map.getMaxZoomLevel()-1));
                            /*
                             * Get the NavigationManager instance.It is responsible for providing voice
                             * and visual instructions while driving and walking
                             */
                            m_navigationManager = NavigationManager.getInstance();
                            positioningManager = PositioningManager.getInstance();
                            m_navigationManager.setMapUpdateMode(NavigationManager.MapUpdateMode.ROADVIEW);
                            m_navigationManager.setRealisticViewMode(NavigationManager.RealisticViewMode.DAY);
                            m_navigationManager.setMap(m_map);
                            try {
                                Image image = new Image();
                                image.setImageResource(R.drawable.location_icon);
                                m_mapFragment.getPositionIndicator().setVisible(true);
                                m_mapFragment.getPositionIndicator().setMarker(image);
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            new AlertDialog.Builder(m_activity).setMessage(
                                    "Error : " + error.name() + "\n\n" + error.getDetails())
                                    .setTitle(R.string.engine_init_error)
                                    .setNegativeButton(android.R.string.cancel,
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    m_activity.finish();
                                                }
                                            }).create().show();
                        }
                    }
                });
            }
        }
    }

    private void createRoute() {
        CoreRouter coreRouter = null;
        try {
            tvEstDistance.setText("");
            tvEstTime.setText("");
            tvNextTurn.setText("");
            m_activity.showLoader("");
            coreRouter = new CoreRouter();
            final RoutePlan routePlan = new RoutePlan();
            RouteOptions routeOptions = new RouteOptions();
            routeOptions.setHighwaysAllowed(false);
            if (vehicleType == 1) {//car = 1
                routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
                routeOptions.setRouteType(RouteOptions.Type.SHORTEST);
                routePlan.setRouteOptions(routeOptions);
            }
            else { //truck
                routeOptions.setTransportMode(RouteOptions.TransportMode.TRUCK);
                routeOptions.setRouteType(RouteOptions.Type.FASTEST);
                routeOptions.setTruckTunnelCategory(RouteOptions.TunnelCategory.E)
                        .setTruckLength(25.25f)
                        .setTruckHeight(2.6f)
                        .setTruckTrailersCount(1);
                routePlan.setRouteOptions(routeOptions);
            }
            routeOptions.setRouteCount(1);
            routePlan.setRouteOptions(routeOptions);

            Image destImage = new Image();
            destImage.setImageResource(R.drawable.image_one);
            MapMarker destMarker = new MapMarker(destLocGeoCordinate = new GeoCoordinate(Constants.Des_Lattitude, Constants.Des_Longitude, 0.0), destImage);
            m_map.addMapObject(destMarker);
            m_map.getPositionIndicator().setSmoothPositionChange(true);
            m_map.getPositionIndicator().setSmoothPositionChange(true);

            RouteWaypoint startPoint = new RouteWaypoint(currentLocGeoCordinate = new GeoCoordinate(Constants.Src_Lattitude, Constants.Src_Longitude));
            RouteWaypoint destination = new RouteWaypoint(destLocGeoCordinate = new GeoCoordinate(Constants.Des_Lattitude, Constants.Des_Longitude));
            routePlan.addWaypoint(startPoint);
            routePlan.addWaypoint(destination);

            /* Trigger the route calculation,results will be called back via the listener */
            coreRouter.calculateRoute(routePlan,
                    new Router.Listener<List<RouteResult>, RoutingError>() {

                        @Override
                        public void onProgress(int i) {
                            /* The calculation progress can be retrieved in this callback. */
                        }

                        @Override
                        public void onCalculateRouteFinished(List<RouteResult> routeResults, RoutingError routingError) {
                            /* Calculation is done.Let's handle the result */
                            m_activity.hideLoader();
                            if (routingError == RoutingError.NONE) {
                                if (routeResults.get(0).getRoute() != null) {
                                    if (m_map != null && mapRoute != null) {
                                        m_map.removeMapObject(mapRoute);
                                        mapRoute = null;
                                    }
                                    m_route                 = routeResults.get(0).getRoute();
                                    mapRoute                = new MapRoute();
                                    mapRoute.setManeuverNumberVisible(true);
                                    mapRoute.setColor(m_activity.getResources().getColor(android.R.color.holo_green_dark));
                                    mapRoute.setOutlineColor(m_activity.getResources().getColor(android.R.color.holo_purple));
                                    mapRoute.setManeuverNumberColor(m_activity.getResources().getColor(android.R.color.holo_orange_dark));
                                    mapRoute.setTraveledColor(m_activity.getResources().getColor(android.R.color.holo_red_dark));
                                    mapRoute.setTrafficEnabled(true);
                                    mapRoute.setRoute(m_route);
                                    mapRoute.setRenderType(MapRoute.RenderType.USER_DEFINED);
                                    m_geoBoundingBox        = m_route.getBoundingBox();
//                                    m_geoBoundingBox.setCoordinates(currentLocGeoCordinate, destLocGeoCordinate);
//                                    m_geoBoundingBox.resizeToCenter(currentLocGeoCordinate);
                                    m_map.addMapObject(mapRoute);
                                    m_map.zoomTo(m_geoBoundingBox, Map.Animation.BOW, Map.MOVE_PRESERVE_ORIENTATION);

                                    /* Create a MapRoute so that it can be placed on the map */
                                    /* Show the maneuver number on top of the route */
                                    /* Add the MapRoute to the map */
                                    /*
                                     * We may also want to make sure the map view is orientated properly
                                     * so the entire route can be easily seen.
                                     */
//                                startNavigation();
                                    TrafficUpdater.getInstance().enableUpdate(true);
//                                    RouteTta routeTta = m_route.getTtaUsingDownloadedTraffic(Route.WHOLE_ROUTE);
//                                    double remDistance = currentLocGeoCordinate.distanceTo(destLocGeoCordinate);//getDestinationDistance();
//                                    if(remDistance > 1000){
//                                        tvEstDistance.setText("Long :  "+decimalFormat.format(remDistance/1000)+" km");
//                                    }
//                                    else {
//                                        tvEstDistance.setText("Long :  "+decimalFormat.format(remDistance)+" mtr");
//                                    }
//
//                                    tvEstTime.setText(""+(routeTta.getDuration()/60) + " min");
//                                    tvNextTurn.setText(m_route.getFirstManeuver().getTurn().name()+" \n "+m_route.getFirstManeuver().getRoadName()+"\n"+m_route.getFirstManeuver().getDistanceToNextManeuver());
                                }
                                else {
                                    Toast.makeText(m_activity, "Error:route results returned is not valid", Toast.LENGTH_LONG).show();
                                }
                            }
                            else {
                                Toast.makeText(m_activity, "Error:No route was found" + routingError, Toast.LENGTH_LONG).show();

//                                Toast.makeText(m_activity, "Error:route calculation returned error code: " + routingError, Toast.LENGTH_LONG).show();
                            }
                        }
                    });

        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initNaviControlButton() {
        m_naviControlButton = (Button) m_activity.findViewById(R.id.naviCtrlButton);
        m_naviControlButton.setText(R.string.start_navi);
        m_naviControlButton.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                /*
                 * To start a turn-by-turn navigation, a concrete route object is required.We use
                 * the same steps from Routing sample app to create a route from 4350 Still Creek Dr
                 * to Langley BC without going on HWY.
                 *
                 * The route calculation requires local map data.Unless there is pre-downloaded map
                 * data on device by utilizing MapLoader APIs,it's not recommended to trigger the
                 * route calculation immediately after the MapEngine is initialized.The
                 * INSUFFICIENT_MAP_DATA error code may be returned by CoreRouter in this case.
                 *
                 */
                if (m_route == null) {
//                    createRoute();
                    Toast.makeText(m_activity, "Please select vehicle type", Toast.LENGTH_SHORT).show();
                }
                else {

                    if(m_naviControlButton.getText().toString().equalsIgnoreCase(m_activity.getResources().getString(R.string.start_navi))){
                        startNavigation();
                    }
                    else {
//                        m_navigationManager.stop();
////                        /*
////                         * Restore the map orientation to show entire route on screen
////                         */
//                        m_map.zoomTo(m_geoBoundingBox, Map.Animation.NONE, 0f);
//                        m_naviControlButton.setText(R.string.start_navi);
                    }
                }
            }
        });
    }

    /*
     * Android 8.0 (API level 26) limits how frequently background apps can retrieve the user's
     * current location. Apps can receive location updates only a few times each hour.
     * See href="https://developer.android.com/about/versions/oreo/background-location-limits.html
     * In order to retrieve location updates more frequently start a foreground service.
     * See https://developer.android.com/guide/components/services.html#Foreground
     */


    private void startNavigation() {
        m_naviControlButton.setText(R.string.stop_navi);
        /* Configure Navigation manager to launch navigation on current map */
        m_navigationManager.setMap(m_map);
        try {
            Image truckImage = new Image();
            if (vehicleType == 1) {
                truckImage.setImageResource(R.drawable.car_marker);// change icon

            }else {
                truckImage.setImageResource(R.drawable.car_marker);// change icon

            }
            m_map.getPositionIndicator().setVisible(true);
            m_map.getPositionIndicator().setMarker(truckImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        if (Constants.Simulate){
//            m_navigationManager.simulate(m_route,30);//Simualtion speed is set to 60 m/s
//        }
//        else {
//            m_map.setTilt(60);
//            m_navigationManager.startNavigation(m_route);
//        }
        m_map.setTilt(60);
        m_navigationManager.startNavigation(m_route);
        /*
         * Set the map update mode to ROADVIEW.This will enable the automatic map movement based on
         * the current location.If user gestures are expected during the navigation, it's
         * recommended to set the map update mode to NONE first. Other supported update mode can be
         * found in HERE Mobile SDK for Android (Premium) API doc
         */
        m_navigationManager.setMapUpdateMode(NavigationManager.MapUpdateMode.ROADVIEW);

        /*
         * NavigationManager contains a number of listeners which we can use to monitor the
         * navigation status and getting relevant instructions.In this example, we will add 2
         * listeners for demo purpose,please refer to HERE Android SDK API documentation for details
         */
        addNavigationListeners();
    }

    private void addNavigationListeners() {
        /*
         * Register a NavigationManagerEventListener to monitor the status change on
         * NavigationManager
         */
        m_navigationManager.addNavigationManagerEventListener(new WeakReference<NavigationManager.NavigationManagerEventListener>(m_navigationManagerEventListener));
        /* Register a PositionListener to monitor the position updates */
        m_navigationManager.addPositionListener( new WeakReference<NavigationManager.PositionListener>(m_positionListener));
    }

    private NavigationManager.PositionListener m_positionListener = new NavigationManager.PositionListener() {
        @Override
        public void onPositionUpdated(GeoPosition geoPosition) {
            /* Current position information can be retrieved in this callback  s = d/t*/
            updateDetails(geoPosition);
        }
    };

    private void updateDetails(GeoPosition geoPosition){
//        Constants.Src_Lattitude = geoPosition.getCoordinate().getLatitude();
//        Constants.Src_Longitude = geoPosition.getCoordinate().getLongitude();
        TrafficUpdater.getInstance().enableUpdate(true);
        long remDistance = (int) (geoPosition.getCoordinate().distanceTo(m_route.getDestination()));//m_navigationManager.getDestinationDistance();//
        RouteTta routeTta = m_route.getTtaIncludingTraffic(Route.WHOLE_ROUTE);
        int speed = (int) geoPosition.getSpeed();// mtr/sec
        long remTime = 0;
        if(speed>0)
        remTime = remDistance/speed;// sec

        StringBuilder stringBuilder = new StringBuilder();

        if(remDistance > 1000){
            tvEstDistance.setText("ETD :  "+decimalFormat.format((remDistance/1000) * 0.62137)+" miles");
        }
        else {
            tvEstDistance.setText("ETD :  "+decimalFormat.format((remDistance*0.000621371))+" miles");
        }
        stringBuilder.append(tvEstDistance.getText().toString()).append("\n");
        tvEstTime.setText("Rem :  "+(remTime/60)+ " min");//+"Speed : "+(speed>0?speed * 18/5:0)+" kmph");//m_navigationManager.getAverageSpeed()
        stringBuilder.append(tvEstTime.getText().toString()).append("\n");
        Maneuver maneuver = m_navigationManager.getNextManeuver();
        if(maneuver !=null){
            String nextTurnDistance = "";
            if(m_navigationManager.getNextManeuverDistance() > 1000){
                nextTurnDistance = decimalFormat.format((m_navigationManager.getNextManeuverDistance()/1000) * 0.62137)+" miles";
            }
            else {
                nextTurnDistance = decimalFormat.format((m_navigationManager.getNextManeuverDistance()*0.000621371))+" miles";
            }

            tvNextTurn.setText(" "+maneuver.getTurn().name()+"\n"+maneuver.getRoadName()+"\n"+nextTurnDistance+"");
            stringBuilder.append(tvNextTurn.getText().toString()).append("\n");
        }

        Log.e("onPositionUpdated ", "Calc : "+stringBuilder.toString());
    }

    private NavigationManager.NavigationManagerEventListener m_navigationManagerEventListener = new NavigationManager.NavigationManagerEventListener() {
        @Override
        public void onRunningStateChanged() {
//            Toast.makeText(m_activity, "Running state changed", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNavigationModeChanged() {
//            Toast.makeText(m_activity, "Navigation mode changed", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onEnded(NavigationManager.NavigationMode navigationMode) {
//            Toast.makeText(m_activity, navigationMode + " was ended", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onMapUpdateModeChanged(NavigationManager.MapUpdateMode mapUpdateMode) {
//            Toast.makeText(m_activity, "Map update mode is changed to " + mapUpdateMode,
//                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRouteUpdated(Route route) {
            if (m_map != null && mapRoute != null) {
                m_map.removeMapObject(mapRoute);
                mapRoute = null;
            }
            m_route = route;
            mapRoute                = new MapRoute();
            mapRoute.setManeuverNumberVisible(true);
            mapRoute.setColor(m_activity.getResources().getColor(android.R.color.holo_green_dark));
            mapRoute.setOutlineColor(m_activity.getResources().getColor(android.R.color.holo_purple));
            mapRoute.setManeuverNumberColor(m_activity.getResources().getColor(android.R.color.holo_orange_dark));
            mapRoute.setTraveledColor(m_activity.getResources().getColor(android.R.color.holo_red_dark));
            mapRoute.setTrafficEnabled(true);
            mapRoute.setRenderType(MapRoute.RenderType.USER_DEFINED);
            mapRoute.setRoute(m_route);
            m_geoBoundingBox        = m_route.getBoundingBox();
            m_map.addMapObject(mapRoute);
            m_map.zoomTo(m_geoBoundingBox, Map.Animation.BOW, Map.MOVE_PRESERVE_ORIENTATION);
//            Toast.makeText(m_activity, "Route updated", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCountryInfo(String s, String s1) {
//            Toast.makeText(m_activity, "Country info updated from " + s + " to " + s1,
//                    Toast.LENGTH_SHORT).show();
        }
    };

    public void onDestroy() {
        /* Stop the navigation when app is destroyed */
        if (m_navigationManager != null) {
            m_navigationManager.stop();
        }
    }
}
