package com.tbs.generic.vansales.collector

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Activitys.BaseActivity
import com.tbs.generic.vansales.Model.CustomerDo
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.PreferenceUtils
import java.util.ArrayList


class CreditInvoiceListActivity : BaseActivity() {
    lateinit var invoiceAdapter: com.tbs.generic.vansales.collector.CreatePaymentAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
    lateinit var view: LinearLayout
    var fromId = 0
    private lateinit var siteListRequest: CrediUnPaidInvoicesListRequest
    var customerid = ""
    private lateinit var btnAddMore: Button
    private var selectedCustomerDOs = ArrayList<CustomerDo>()

    override fun onResume() {
        selectInvoiceList()
        super.onResume()
    }

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.credit_invoice_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        tvScreenTitle.text = getString(R.string.select_invoice)
        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById<TextView>(R.id.tvNoDataFound)
        view = findViewById<LinearLayout>(R.id.view)
        btnAddMore = findViewById<Button>(R.id.btnAddMore)

        recycleview.layoutManager = linearLayoutManager

        selectInvoiceList()
        if (intent.hasExtra("CODE")) {
            customerid = intent.extras?.getString("CODE").toString()
        }
        val btnCreate = findViewById<Button>(R.id.btnCreate)

        btnCreate.setOnClickListener {
            val intent = Intent(this@CreditInvoiceListActivity, com.tbs.generic.vansales.collector.CreatePaymentActivity::class.java)
            intent.putExtra("Sales", 1)
            intent.putExtra("CODE", customerid)

            startActivity(intent)
        }
        btnAddMore.setOnClickListener {
            if (invoiceAdapter != null) {
                val selectedUnPaidInvoiceDOS = invoiceAdapter.getselectedUnPaidInvoiceDOS()
                if (selectedUnPaidInvoiceDOS != null && !selectedUnPaidInvoiceDOS.isEmpty()) {
                    val intent = Intent(this@CreditInvoiceListActivity, CreditInvoiceListActivity::class.java)
                    intent.putExtra("selectedInvoiceDOs", selectedUnPaidInvoiceDOS)
                    startActivityForResult(intent, 11)
                } else {
                    showToast(getString(R.string.please_select_inovice))
                }
            } else {
                showToast(getString(R.string.no_invoices_found))
            }


        }
    }

    private fun selectInvoiceList() {


//        var tvInvoiceId = findViewById(R.id.tvInvoiceId) as TextView
        var tvNoData = findViewById<TextView>(R.id.tvNoDataFound)
        var tvAmount = findViewById<TextView>(R.id.tvAmount)


        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)

        if (intent.hasExtra("CODE")) {
            customerid = intent.extras?.getString("CODE")!!
        }
        if (intent.hasExtra("selectedCustomerDOs")) {
            selectedCustomerDOs = intent.getSerializableExtra("selectedCustomerDOs") as ArrayList<CustomerDo>
        }
        val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, resources.getString(R.string.checkin_non_scheduled))

        siteListRequest = CrediUnPaidInvoicesListRequest(selectedCustomerDOs, this@CreditInvoiceListActivity)

        siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
            hideLoader()
            if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {

//                    tvInvoiceId.setVisibility(View.VISIBLE)
                tvAmount.visibility = View.VISIBLE
                tvAmount.text = "" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency

                preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, String.format("%.2f", unPaidInvoiceMainDO.totalAmount))
                preferenceUtils.saveInt(PreferenceUtils.PAYMENT_TYPE, unPaidInvoiceMainDO.type)


                tvNoData.visibility = View.GONE
                unPaidInvoiceMainDO
                if (isError) {
                    Toast.makeText(this@CreditInvoiceListActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                } else {
                    tvAmount.visibility = View.VISIBLE
                    view.visibility = View.VISIBLE

                    invoiceAdapter = com.tbs.generic.vansales.collector.CreatePaymentAdapter(this@CreditInvoiceListActivity, unPaidInvoiceMainDO.unPaidInvoiceDOS, customerid, "CREDIT")
                    recycleview.adapter = invoiceAdapter
                    tvAmount.text = "" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency
                    preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, "" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount))

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)f

                }
            } else {
//                tvInvoiceId.setVisibility(View.VISIBLE)
                tvAmount.visibility = View.GONE

                tvNoData.visibility = View.VISIBLE
                recycleview.visibility = View.GONE
                view.visibility = View.GONE

                hideLoader()

            }

        }

        siteListRequest.execute()


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
            selectInvoiceList()
        }
    }


}