package com.alert.rajdialogs;

import android.content.Context;
import android.graphics.PorterDuff;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alert.rajdialogs.interfaces.Closure;


/**
 * Created by rajkumar on 23/08/17.
 */

public class SuccessDialog extends DialogBuilder<SuccessDialog> {

    private Button positiveButton;
    private Button negativeButton;
    private Button doneButton;
    private RelativeLayout dialogBody;
    private TextView dialogTitle;

    public SuccessDialog(Context context) {
        super(context);

        setColoredCircle(R.color.dialogSuccessBackgroundColor);
        setDialogIconAndColor(R.drawable.ic_success, R.color.white);
        setNegativeButtonbackgroundColor(R.color.dialogSuccessBackgroundColor);
        setPositiveButtonbackgroundColor(R.color.dialogSuccessBackgroundColor);
        setDoneButtonbackgroundColor(R.color.dialogSuccessBackgroundColor);
    }


    {
        positiveButton = findView(R.id.btDialogYes);
        negativeButton = findView(R.id.btDialogNo);
        doneButton = findView(R.id.btDialogDone);
        dialogBody = findView(R.id.dialog_body);
        dialogTitle = findView(R.id.dialog_title);
    }

    public SuccessDialog setDialogBodyBackgroundColor(int color) {
        if (dialogBody != null) {
            dialogBody.getBackground().setColorFilter(ContextCompat.getColor(getContext(), color), PorterDuff.Mode.SRC_IN);
        }

        return this;
    }

    public SuccessDialog setPositiveButtonClick(@Nullable final Closure selectedYes) {
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedYes != null) {
                    selectedYes.exec();
                }

                hide();
            }
        });

        return this;
    }

    public SuccessDialog setNegativeButtonClick(@Nullable final Closure selectedNo) {
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedNo != null) {
                    selectedNo.exec();
                }

                hide();
            }
        });

        return this;
    }

    public SuccessDialog setDoneButtonClick(@Nullable final Closure selectedDone) {
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedDone != null) {
                    selectedDone.exec();
                }

                hide();
            }
        });

        return this;
    }

    public SuccessDialog showPositiveButton(boolean show) {
        if (positiveButton != null) {
            positiveButton.setVisibility(View.VISIBLE);
        }

        return this;
    }

    public SuccessDialog showNegativeButton(boolean show) {
        if (negativeButton != null) {
            negativeButton.setVisibility(View.VISIBLE);
        }

        return this;
    }

    public SuccessDialog showDoneButton(boolean show) {
        if (doneButton != null) {
            doneButton.setVisibility(View.VISIBLE);
        }

        return this;
    }

    public SuccessDialog setPositiveButtonbackgroundColor(int buttonBackground) {
        if (positiveButton != null) {
            positiveButton.getBackground().setColorFilter(ContextCompat.getColor(getContext(), buttonBackground), PorterDuff.Mode.SRC_IN);
        }

        return this;
    }

    public SuccessDialog setPositiveButtonTextColor(int textColor) {
        if (positiveButton != null) {
            positiveButton.setTextColor(ContextCompat.getColor(getContext(), textColor));
        }

        return this;
    }

    public SuccessDialog setPositiveButtonText(String text) {
        if (positiveButton != null) {
            positiveButton.setText(text);
            positiveButton.setVisibility(View.VISIBLE);
        }

        return this;
    }

    public SuccessDialog setNegativeButtonbackgroundColor(int buttonBackground) {
        if (negativeButton != null) {
            negativeButton.getBackground().setColorFilter(ContextCompat.getColor(getContext(), buttonBackground), PorterDuff.Mode.SRC_IN);
        }

        return this;
    }

    public SuccessDialog setNegativeButtonText(String text) {
        if (negativeButton != null) {
            negativeButton.setText(text);
            negativeButton.setVisibility(View.VISIBLE);
        }

        return this;
    }

    public SuccessDialog setNegativeButtonTextColor(int textColor) {
        if (negativeButton != null) {
            negativeButton.setTextColor(ContextCompat.getColor(getContext(), textColor));
        }

        return this;
    }

    public SuccessDialog setDoneButtonbackgroundColor(int buttonBackground) {
        if (doneButton != null) {
            doneButton.getBackground().setColorFilter(ContextCompat.getColor(getContext(), buttonBackground), PorterDuff.Mode.SRC_IN);
        }

        return this;
    }

    public SuccessDialog setDoneButtonText(String text) {
        if (doneButton != null) {
            doneButton.setText(text);
            doneButton.setVisibility(View.VISIBLE);
        }

        return this;
    }

    public SuccessDialog setDoneButtonTextColor(int textColor) {
        if (doneButton != null) {
            doneButton.setTextColor(ContextCompat.getColor(getContext(), textColor));
        }

        return this;
    }

    public SuccessDialog setTitleText(String text) {
        if (dialogTitle != null) {
            dialogTitle.setText(text);
        }

        return this;
    }

    @Override
    protected int getLayout() {
        return R.layout.dialog_success;
    }
}
